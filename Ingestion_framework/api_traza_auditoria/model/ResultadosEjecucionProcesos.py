from datetime import datetime
from pyspark.sql.types import DateType, IntegerType, StringType, StructField, StructType, TimestampType


class ResultadosEjecucionProcesos:
    _id_proceso = ''
    _fecha_ejecucion = datetime.now()
    _hora_inicio = datetime.now()
    _hora_fin = None
    _resultado = 'RUNNING'
    _error = ''
    _id_resultado_ejecucion_proceso = ''
    _nombre_proceso = ''

    def __init__(self, proceso, nombre_proceso):
        self.set_id_proceso(proceso)
        self.set_nombre_proceso(nombre_proceso)

    def get_id_proceso(self):
        return self._id_proceso

    def get_fecha_ejecucion(self):
        return self._fecha_ejecucion

    def get_hora_inicio(self):
        return self._hora_inicio

    def get_hora_fin(self):
        return self._hora_fin

    def get_resultado(self):
        return self._resultado

    def get_error(self):
        return self._error

    def get_id_resultado_ejecucion_proceso(self):
        return self._id_resultado_ejecucion_proceso

    def get_nombre_proceso(self):
        return self._nombre_proceso

    def set_id_proceso(self, id_proceso):
        self._id_proceso = id_proceso

    def set_fecha_ejecucion(self, fecha_ejecucion):
        self._fecha_ejecucion = fecha_ejecucion

    def set_hora_inicio(self, hora_inicio):
        self._hora_inicio = hora_inicio

    def set_hora_fin(self, hora_fin):
        self._hora_fin = hora_fin

    def set_resultado(self, resultado):
        self._resultado = resultado

    def set_error(self, error):
        self._error = error

    def set_id_resultado_ejecucion_proceso(self, id_resultado_ejecucion_proceso):
        self._id_resultado_ejecucion_proceso = id_resultado_ejecucion_proceso

    def set_nombre_proceso(self, nombre_proceso):
        self._nombre_proceso = nombre_proceso


