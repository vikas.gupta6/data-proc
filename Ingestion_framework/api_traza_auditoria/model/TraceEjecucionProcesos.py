from datetime import datetime


class TraceEjecucionProcesos:
    _id_proceso = ''
    _id_resultado_ejecucion_proceso = ''
    _orden = -1
    _descripcion = ''
    _codigo_ejecutado = ''
    _hora_ejecucion = None

    def __init__(self, proceso):
        self.set_id_proceso(proceso)
        self.set_hora_ejecucion(datetime.now())

    def get_id_proceso(self):
        return self._id_proceso

    def get_id_resultado_ejecucion_proceso(self):
        return self._id_resultado_ejecucion_proceso

    def get_orden(self):
        return self._orden

    def get_descripcion(self):
        return self._descripcion

    def get_codigo_ejecutado(self):
        return self._codigo_ejecutado

    def get_hora_ejecucion(self):
        return self._hora_ejecucion

    def set_id_proceso(self, id_proceso):
        self._id_proceso = id_proceso

    def set_id_resultado_ejecucion_proceso(self, id_resultado_ejecucion_proceso):
        self._id_resultado_ejecucion_proceso = id_resultado_ejecucion_proceso

    def set_orden(self, orden):
        self._orden = orden

    def set_descripcion(self, descripcion):
        self._descripcion = descripcion

    def set_codigo_ejecutado(self, codigo_ejecutado):
        self._codigo_ejecutado = codigo_ejecutado

    def set_hora_ejecucion(self, hora_ejecucion):
        self._hora_ejecucion = hora_ejecucion