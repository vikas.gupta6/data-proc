from ..IResultadosEjecucionProcesosDAO import IResultadosEjecucionProcesosDAO
from pyspark.sql.types import DateType, StringType, StructField, StructType, TimestampType
import os


class BigqueryResultadosEjecucionProcesosDAO(IResultadosEjecucionProcesosDAO):
    _schema = StructType([StructField('id_proceso', StringType()),
                          StructField('nombre_proceso', StringType()),
                          StructField('fecha_ejecucion', DateType()),
                          StructField('hora_inicio', TimestampType()),
                          StructField('hora_fin', TimestampType()),
                          StructField('resultado', StringType()),
                          StructField('error', StringType()),
                          StructField('id_resultado_ejecucion_proceso', StringType())
                          ])

    def __init__(self, verbose=False):
        super(BigqueryResultadosEjecucionProcesosDAO, self).__init__(verbose=verbose)
        if 'nombre_resultados_ejecucion_procesos' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_resultados_ejecucion_procesos'])

    def insert(self, resultados_ejecucion_procesos):
        resultados_ejecucion_procesos.set_id_resultado_ejecucion_proceso(
            self.get_spark_context().applicationId)
        data = [(
            resultados_ejecucion_procesos.get_id_proceso(),
            resultados_ejecucion_procesos.get_nombre_proceso(),
            resultados_ejecucion_procesos.get_fecha_ejecucion(),
            resultados_ejecucion_procesos.get_hora_inicio(),
            resultados_ejecucion_procesos.get_hora_fin(),
            resultados_ejecucion_procesos.get_resultado(),
            resultados_ejecucion_procesos.get_error(),
            resultados_ejecucion_procesos.get_id_resultado_ejecucion_proceso()
        ), ]
        self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["temp_bucket"])
        dataframe_insert = self.get_sql_context().createDataFrame(
            data, schema=self.get_schema())

        if self.is_verbose():
            print('Dataframe Insertar en tabla {tax}.{tabla}'.format(
                tax=self.get_config()['TAXONOMIA'], tabla=self.get_nombre_objeto()))
            dataframe_insert.show(20, False)

            # dataframe_insert.write.partitionBy('id_resultado_ejecucion_proceso').insertInto(
        #     '{tax}.{tabla}'.format(tax=self.get_config()['TAXONOMIA'], tabla=self.get_nombre_objeto()), overwrite=True)

        cmm = "bq query --nouse_legacy_sql "
        del_query = "delete from {project}.{dataset}.{table} " \
                    "where id_resultado_ejecucion_proceso = '{execution_id}'".format(
            project=self.get_config()['project'],
            dataset=self.get_config()['TAXONOMIA'],
            table=self.get_nombre_objeto(),
            execution_id=self.get_spark_context().applicationId
        )

        cmm = cmm + '"' + del_query + '"'
        print(cmm)
        os.system(cmm)

        dataframe_insert.write.format("bigquery").option("writeMethod", "direct").mode("Append") \
            .save('{tax}.{tabla}'.format(tax=self.get_config()['TAXONOMIA'],
                                         tabla=self.get_nombre_objeto()))
