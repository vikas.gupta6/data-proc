from ..ITraceEjecucionProcesosDAO import ITraceEjecucionProcesosDAO
from pyspark.sql.types import IntegerType, StringType, StructField, StructType, TimestampType


class BigqueryTraceEjecucionProcesosDAO(ITraceEjecucionProcesosDAO):
    _schema = StructType([StructField('id_proceso', StringType()),
                          StructField('id_resultado_ejecucion_proceso', StringType()),
                          StructField('orden', IntegerType()),
                          StructField('hora_ejecucion', TimestampType()),
                          StructField('descripcion', StringType()),
                          StructField('codigo_ejecutado', StringType())
                          ])

    def __init__(self, verbose=False):
        super(BigqueryTraceEjecucionProcesosDAO, self).__init__(verbose=verbose)
        if 'nombre_trace_ejecucion_procesos' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_trace_ejecucion_procesos'])

    def insert(self, trace_ejecucion_procesos):
        trace_ejecucion_procesos.set_id_resultado_ejecucion_proceso(
            self.get_spark_context().applicationId)
        data = [(
            trace_ejecucion_procesos.get_id_proceso(),
            trace_ejecucion_procesos.get_id_resultado_ejecucion_proceso(),
            trace_ejecucion_procesos.get_orden(),
            trace_ejecucion_procesos.get_hora_ejecucion(),
            trace_ejecucion_procesos.get_descripcion(),
            trace_ejecucion_procesos.get_codigo_ejecutado()
        )]
        self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["temp_bucket"])
        dataframe_insert = self.get_sql_context().createDataFrame(
            data, schema=self.get_schema())

        if self.is_verbose():
            print('Dataframe Insertar en tabla {tax}.{tabla}'.format(
                tax=self.get_config()['TAXONOMIA'], tabla=self.get_nombre_objeto()))
            dataframe_insert.show(20, False)

        # dataframe_insert.write.insertInto('{tax}.{tabla}'.format(tax=self.get_config()['TAXONOMIA'], tabla=self.get_nombre_objeto()))
        dataframe_insert.write.format('bigquery').option("writeMethod", "direct").mode("Append") \
            .save('{tax}.{tabla}'.format(tax=self.get_config()['TAXONOMIA'],
                                         tabla=self.get_nombre_objeto()))
