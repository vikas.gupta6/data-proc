from datetime import datetime
# from model.DAO.hive.HiveResultadosEjecucionProcesosDAO import HiveResultadosEjecucionProcesosDAO
# from model.DAO.hive.HiveTraceEjecucionProcesosDAO import HiveTraceEjecucionProcesosDAO
from Ingestion_framework.api_traza_auditoria.model.DAO.bigquery.BigqueryResultadosEjecucionProcesosDAO import \
    BigqueryResultadosEjecucionProcesosDAO
from Ingestion_framework.api_traza_auditoria.model.DAO.bigquery.BigqueryTraceEjecucionProcesosDAO \
    import BigqueryTraceEjecucionProcesosDAO
from Ingestion_framework.api_traza_auditoria.model.ResultadosEjecucionProcesos \
    import ResultadosEjecucionProcesos
from Ingestion_framework.api_traza_auditoria.model.TraceEjecucionProcesos \
    import TraceEjecucionProcesos


class TraceEjecucionProcesosManager:
    resultados_ejecucion_procesos_dao = None
    trace_ejecucion_procesos_dao = None

    resultados_ejecucion_procesos = None
    trace_ejecucion_procesos = None

    _id_framework = None
    _orden = -1
    _proceso = None
    _verbose = False
    _verbose_dao = False
    _nombre_proceso = ''

    def __init__(self, framework, proceso, verbose=False, nombre_proceso='', verbose_dao=False):
        self.set_id_framework(framework)
        self.set_proceso(proceso)
        self.set_verbose(verbose)
        self.set_nombre_proceso(nombre_proceso)
        self.set_verbose_dao(verbose_dao)

        self.resultados_ejecucion_procesos = ResultadosEjecucionProcesos(self.get_proceso(),
                                                                         self.get_nombre_proceso())
        # self.resultados_ejecucion_procesos_dao = HiveResultadosEjecucionProcesosDAO()
        self.resultados_ejecucion_procesos_dao = BigqueryResultadosEjecucionProcesosDAO()
        self.resultados_ejecucion_procesos_dao.set_verbose(verbose)

        # self.trace_ejecucion_procesos_dao = HiveTraceEjecucionProcesosDAO()
        self.trace_ejecucion_procesos_dao = BigqueryTraceEjecucionProcesosDAO()
        self.trace_ejecucion_procesos_dao.set_verbose(verbose)

        self.set_orden(1)

    def __enter__(self):
        tepm = TraceEjecucionProcesosManager(
            self.get_id_framework(), self.get_proceso(), self.is_verbose(), self.get_nombre_proceso())
        tepm.insert_resultado_proceso()
        return tepm

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.resultados_ejecucion_procesos.set_hora_fin(datetime.now())
        if exc_type is None:
            self.resultados_ejecucion_procesos.set_resultado('SUCCESS')
        else:
            self.resultados_ejecucion_procesos.set_resultado('ERROR')
            self.resultados_ejecucion_procesos.set_error(str(exc_value).replace('\n', ' '))
        self.insert_resultado_proceso()

    def insert_trace_ejecucion_proceso(self, descripcion, codigo_ejecutado=''):
        self.trace_ejecucion_procesos = TraceEjecucionProcesos(self.get_proceso())
        self.trace_ejecucion_procesos.set_descripcion(descripcion)
        self.trace_ejecucion_procesos.set_codigo_ejecutado(codigo_ejecutado)
        self.trace_ejecucion_procesos.set_orden(self.get_orden())

        self.trace_ejecucion_procesos_dao.insert(self.trace_ejecucion_procesos)

        self.set_orden(self.get_orden() + 1)

    def insert_resultado_proceso(self):
        self.resultados_ejecucion_procesos_dao.insert(self.resultados_ejecucion_procesos)

    def set_id_framework(self, id_framework):
        self._id_framework = id_framework

    def set_proceso(self, proceso):
        self._proceso = proceso

    def set_orden(self, orden):
        self._orden = orden

    def set_verbose(self, verbose):
        self._verbose = verbose

    def set_verbose_dao(self, verbose_dao):
        self._verbose_dao = verbose_dao

    def set_nombre_proceso(self, nombre_proceso):
        self._nombre_proceso = nombre_proceso

    def get_id_framework(self):
        return self._id_framework

    def get_proceso(self):
        return self._proceso

    def is_verbose(self):
        return self._verbose

    def is_verbose_dao(self):
        return self._verbose_dao

    def get_orden(self):
        return self._orden

    def get_nombre_proceso(self):
        return self._nombre_proceso