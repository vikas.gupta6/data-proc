import subprocess as sp
import json
from google.cloud import storage
from pyspark import SparkContext, HiveContext
import os
import csv
import random
import time


class Utiles(object):
    _config = None
    _properties = None
    _spark_context = None
    _sql_context = None
    storage_client = storage.Client()

    # def mover_archivo(self, dest_folder, source_bucket, file_name):
    #     source_bucket_ = self.storage_client.get_bucket(source_bucket)
    #     source_blob = source_bucket_.blob(file_name)
    #     dest_file = dest_folder + file_name.split('/')[-1]
    #     if self.es_archivo(source_bucket, file_name):
    #         source_bucket_.copy_blob(source_blob, source_bucket_, new_name=dest_file)
    #         source_bucket_.delete_blob(source_blob.name)

    def mover_archivo(self, dest_folder, source_bucket, nombre_archivo_origen , nombre_archivo_destino=False):
        if not nombre_archivo_destino:
            nombre_archivo_destino = nombre_archivo_origen
        print("###")
        source_bucket_ = self.storage_client.get_bucket(source_bucket)
        print(source_bucket_)
        source_blob = source_bucket_.blob(nombre_archivo_destino)
        print(source_blob)
        dest_file = dest_folder + nombre_archivo_destino.split('/')[-1]
        print(dest_file)
        if self.es_archivo(source_bucket, nombre_archivo_destino):
            source_bucket_.copy_blob(source_blob, source_bucket_, new_name=dest_file)
            source_bucket_.delete_blob(source_blob.name)

    def es_archivo(self, bucket_name, file_name):
        # if file_name.startswith('/'):
        #     file_name = file_name[1:]
        bucket = self.storage_client.bucket(bucket_name)
        print("file_name in utiles{}, bucket_name in utiles {} ".format(file_name,bucket_name))
        return storage.Blob(bucket=bucket, name=file_name).exists(self.storage_client)

    def crear_carpeta(self, bucket_name, folder_path):
        print(folder_path)
        if not self.es_archivo(bucket_name, folder_path ):
            bucket = self.storage_client.get_bucket(bucket_name)
            blob = bucket.blob(folder_path)
            blob.upload_from_string('')
            print('Created {} .'.format(folder_path))

    @staticmethod
    def load_config(config_file):
        config = None
        if config_file.split(':')[0] == 'gs':
            client = storage.Client()
            bucket = client.get_bucket(config_file.split('//')[1].split('/')[0])
            blob = bucket.blob('/'.join(config_file.split('//')[1].split('/')[1:]))
            config = json.loads(blob.download_as_string())
        else:
            with open(config_file) as f:
                config = json.load(f)
        return config

    # def get_gcs_fileslist(self, bucket_name, folder_name):
    #     source_bucket = self.storage_client.get_bucket(bucket_name)
    #     folder_name = folder_name + '/'
    #     blobs = source_bucket.list_blobs(prefix=folder_name)
    #     blobs_return = []
    #     for blob in blobs:
    #         if blob.name.split('/')[-1] != '':
    #             blobs_return.append(blob.name.split('/')[-1])
    #     return blobs_return

    @staticmethod
    def get_gcs_files_list(bucket_name, folder_name):
        file_list = []
        if not folder_name.endswith('/'):
            folder_name = folder_name + '/'
        if not folder_name.startswith('/'):
            folder_name = '/' + folder_name
        gcs_uri = 'gs://' + bucket_name + folder_name
        cmd = 'gsutil ls -l ' + gcs_uri
        gcs_file_list = sp.getoutput(cmd)

        for files in gcs_file_list.split('\n'):
            if 'gs://' in files:
                temp = files.split(gcs_uri)[1]
                if ('/' in temp) or (temp == ''):
                    pass
                else:
                    file_list.append(temp)
        return file_list

    def get_spark_session(self):
        if self._spark_context is None:
            self._spark_context = SparkContext.getOrCreate()
            self._spark_context.setLogLevel('WARN')
        if self._sql_context is None:
                self._sql_context = HiveContext(self._spark_context)
                self._sql_context.setConf("hive.exec.dynamic.partition", "true")
                self._sql_context.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
        return self._spark_context, self._sql_context



    def salvar_archivo(self, bucket_name, df, nombre_archivo_destino, path_folder_destino, delimitador, folder_temporal):
        carpeta_temporal_archivo = nombre_archivo_destino.split(".")[0]
        print("Inside salvar archivo, carpeta_temporal_archivo {}".format(carpeta_temporal_archivo))
        path_archivo_destino = path_folder_destino+'/'+nombre_archivo_destino
        print("Inside salvar archivo, path_archivo_destino {}".format(path_archivo_destino))
        path_carpeta_temporal = path_folder_destino+'/'+folder_temporal+'/'+carpeta_temporal_archivo
        print("Inside salvar archivo, path_carpeta_temporal {}".format(path_carpeta_temporal))
        path_archivo_temporal = path_carpeta_temporal+'/'+"part*"
        print("Inside salvar archivo, path_archivo_temporal {}".format(path_archivo_temporal))
        Utiles().eliminar_archivo(bucket_name , ruta_archivo=path_folder_destino+'/'+folder_temporal+'/',archivo=carpeta_temporal_archivo)
        
        if delimitador is not None and delimitador != '':
            print("Inside salvar archivo, if df {}".format(df))
            df.show()

            # df.write.format("csv").option("path", "gs://"+bucket_name+"/"+path_carpeta_temporal).option('delimiter', delimitador).option("nullValue", "").option(
            #     'quoteMode', 'NONE').option('escape', '\\').save(emptyValue='')

            df.repartition(1).write.format('csv').option('delimiter', delimitador).option("nullValue", "").option(
                'quoteMode', 'NONE').option('escape', '\\').save("gs://"+bucket_name+"/"+path_carpeta_temporal, emptyValue='')        	
        else:
            # df.repartition(1).write.mode('default').text("path", "gs://"+bucket_name+path_carpeta_temporal)
            print("Inside salvar archivo, else df {}".format(df))
            df.show()
            
            # df.write.format("text").option("path", "gs://"+bucket_name+"/"+path_carpeta_temporal)
            df.repartition(1).write.format('text').save("gs://"+bucket_name+"/"+path_carpeta_temporal)
        	            
        	
        print("salvar end")
        Utiles().eliminar_archivo(bucket_name, ruta_archivo=path_folder_destino, archivo=nombre_archivo_destino)
        Utiles().mover_archivo(path_archivo_destino, bucket_name, path_archivo_temporal)
        # cmm = "gsutil mv gs://{0} {1}".format(path_archivo_temporal, path_archivo_destino)
        # print("Inside salvar archivo, cmm {}".format(cmm))
        # os.system(cmm)
        Utiles().eliminar_archivo(bucket_name, folder_temporal, path_folder_destino)

    def eliminar_archivo(self, bucket_name, archivo, ruta_archivo):
        path_archivo = bucket_name+'/'+ruta_archivo+archivo
        print("Inside eliminar_archivo, path_archivo {}".format(path_archivo))
        cmm = "gsutil rm -r gs://{0}".format(path_archivo)
        print("Inside eliminar_archivo, cmm {}".format(cmm))
        return os.system(cmm)

    def cargar_archivo(self, bucket_name, archivo, folder, delimitador,multilinea=False, encoding='utf-8'):
        sql_context, spark_context = Utiles().get_spark_session()
        sq = spark_context.getOrCreate(sql_context)
        ruta_hdfs_archivo = 'gs://'+bucket_name+'/'+folder + archivo
        print ("This is ruta_hdfs_archivo {}".format(ruta_hdfs_archivo))
        print("This is delimitador {}". format(delimitador))
        if delimitador is not None and delimitador != '':
            print("we are in if utiles")
            parcial = sq.read.format('csv').option('delimiter', delimitador).option('charset',encoding)
            print("This is parcial {}".format(parcial))
            if multilinea:
                parcial.option('multiLine', True)
            df = parcial.load(ruta_hdfs_archivo)
            print("This is utiles df if {}".format(df))
            df.show()
        else:
            print("we are in else utiles")
            df = sq.read.text(ruta_hdfs_archivo)
            print("This is utiles df else {}".format(df))
            df.show()

        return df

    # def cargar_archivo(self, bucket_name,archivo, folder, delimitador,multilinea=False, encoding='utf-8'):
    #     from pyspark.sql import SparkSession
    #
    #     appName = "PySpark Example - Read CSV file from GCS"
    #     master = "local"
    #
    #     # Create Spark session
    #     spark = SparkSession.builder \
    #         .appName(appName) \
    #         .master(master) \
    #         .getOrCreate()
    #
    #     ruta_hdfs_archivo = 'gs://'+bucket_name+'/'+folder + archivo
    #     print("This is ruta_hdfs_archivo {}".format(ruta_hdfs_archivo))
    #     print("This is delimatador {}".format(delimitador))
    #     # return ruta_hdfs_archivo
    #     if delimitador is not None and delimitador != '':
    #         # self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["source_bucket"])
    #         parcial = spark.read.option("header", "true").csv(ruta_hdfs_archivo).option('delimiter', delimitador).option('charset',encoding)
    #         print("This is parcial {}".format(parcial))
    #         if multilinea:
    #             parcial.option('multiLine', True)
    #         df = parcial.load(ruta_hdfs_archivo)
    #         print("This is df {}".format(df))
    #     else:
    #         df = spark.read.text(ruta_hdfs_archivo)
    #         df.show()
    #         print("Didn't go to if {}".format(df))
    #     return df

