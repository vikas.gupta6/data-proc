from Ingestion_framework.framework_limpieza_transformacion.fabricas.DAOFactory import \
    DAOFactory
from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.bigquery.BigQueryFuentesClouderaDAO import \
    BigQueryFuentesClouderaDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.bigquery.BigQueryCamposFuentesClouderaDAO import \
    BigQueryCamposFuentesClouderaDAO
# from modelo.DAO.bigquery.BigQueryAsociacionArchivoFuenteDAO import BigQueryAsociacionArchivoFuenteDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.bigquery.BigQueryArchivoFuenteDAO import \
    BigQueryArchivoFuenteDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.bigquery.BigQueryTransformacionArchivoDAO import \
    BigQueryTransformacionArchivoDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.bigquery.BigQueryParametroTransformacionDAO import \
    BigQueryParametroTransformacionDAO

class BigQueryDAOFactory(DAOFactory):

    def obtener_fuentes_cloudera_dao(self):
        if self._fuentes_cloudera_dao is None:
            self._fuentes_cloudera_dao = BigQueryFuentesClouderaDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._fuentes_cloudera_dao
    
    def obtener_campos_fuentes_cloudera_dao(self):
        if self._campos_fuentes_cloudera_dao is None:
            self._campos_fuentes_cloudera_dao = BigQueryCamposFuentesClouderaDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._campos_fuentes_cloudera_dao
    #
    # def obtener_asociacion_archivo_fuente_dao(self):
    #     if self._asociacion_archivo_fuente_dao is None:
    #         self._asociacion_archivo_fuente_dao = BigQueryAsociacionArchivoFuenteDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
    #     return  self._asociacion_archivo_fuente_dao
    #
    def obtener_archivo_fuente_dao(self):
        if self._archivo_fuente_dao is None:
            self._archivo_fuente_dao = BigQueryArchivoFuenteDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        print("Reached BQDAOFactory")
        return self._archivo_fuente_dao
    #
    def obtener_transformacion_archivo_dao(self):
        if self._transformacion_archivo_dao is None:
            self._transformacion_archivo_dao = BigQueryTransformacionArchivoDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._transformacion_archivo_dao

    def obtener_parametro_transformacion_dao(self):
        if self._parametro_transformacion_dao is None:
            self._parametro_transformacion_dao = BigQueryParametroTransformacionDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._parametro_transformacion_dao
