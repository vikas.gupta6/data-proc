from DAOFactory import DAOFactory
from modelo.DAO.hive.HiveFuentesClouderaDAO import HiveFuentesClouderaDAO
from modelo.DAO.hive.HiveCamposFuentesClouderaDAO import HiveCamposFuentesClouderaDAO
from modelo.DAO.hive.HiveAsociacionArchivoFuenteDAO import HiveAsociacionArchivoFuenteDAO
from modelo.DAO.hive.HiveArchivoFuenteDAO import HiveArchivoFuenteDAO
from modelo.DAO.hive.HiveTransformacionArchivoDAO import HiveTransformacionArchivoDAO
from modelo.DAO.hive.HiveParametroTransformacionDAO import HiveParametroTransformacionDAO

class HiveDAOFactory(DAOFactory):

    def obtener_fuentes_cloudera_dao(self):
        if self._fuentes_cloudera_dao is None:
            self._fuentes_cloudera_dao =  HiveFuentesClouderaDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._fuentes_cloudera_dao
    
    def obtener_campos_fuentes_cloudera_dao(self):
        if self._campos_fuentes_cloudera_dao is None:
            self._campos_fuentes_cloudera_dao = HiveCamposFuentesClouderaDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._campos_fuentes_cloudera_dao
    
    def obtener_asociacion_archivo_fuente_dao(self):
        if self._asociacion_archivo_fuente_dao is None:
            self._asociacion_archivo_fuente_dao = HiveAsociacionArchivoFuenteDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._asociacion_archivo_fuente_dao
    
    def obtener_archivo_fuente_dao(self):
        if self._archivo_fuente_dao is None:
            self._archivo_fuente_dao = HiveArchivoFuenteDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        print("Reached HiveDAOFactory")
        return self._archivo_fuente_dao
    
    def obtener_transformacion_archivo_dao(self):
        if self._transformacion_archivo_dao is None:
            self._transformacion_archivo_dao = HiveTransformacionArchivoDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._transformacion_archivo_dao
    
    def obtener_parametro_transformacion_dao(self):
        if self._parametro_transformacion_dao is None:
            self._parametro_transformacion_dao = HiveParametroTransformacionDAO(trace_ejecucion_procesos_manager=self.get_trace_ejecucion_procesos_manager())
        return self._parametro_transformacion_dao
    