
from abc import abstractmethod


class DAOFactory:

    _fuentes_cloudera_dao = None
    _campos_fuentes_cloudera_dao = None
    _asociacion_archivo_fuente_dao = None
    _archivo_fuente_dao = None
    _transformacion_archivo_dao = None
    _parametro_transformacion_dao = None
    _trace_ejecucion_procesos_manager = None

    def __init__(self, trace_ejecucion_procesos_manager = None):
        self.set_trace_ejecucion_procesos_manager(trace_ejecucion_procesos_manager)

    @abstractmethod
    def obtener_fuentes_cloudera_dao(self, trace_ejecucion_procesos_manager = None):
        pass
    
    @abstractmethod
    def obtener_campos_fuentes_cloudera_dao(self, trace_ejecucion_procesos_manager = None):
        pass
    
    @abstractmethod
    def obtener_asociacion_archivo_fuente_dao(self, trace_ejecucion_procesos_manager = None):
        pass
    
    @abstractmethod
    def obtener_archivo_fuente_dao(self, trace_ejecucion_procesos_manager = None):
        pass
    
    @abstractmethod
    def obtener_transformacion_archivo_dao(self, trace_ejecucion_procesos_manager = None):
        pass
    
    @abstractmethod
    def obtener_parametro_transformacion_dao(self, trace_ejecucion_procesos_manager = None):
        pass

    def set_trace_ejecucion_procesos_manager(self, trace_ejecucion_procesos_manager):
        self._trace_ejecucion_procesos_manager = trace_ejecucion_procesos_manager

    def get_trace_ejecucion_procesos_manager(self):
        return self._trace_ejecucion_procesos_manager
    