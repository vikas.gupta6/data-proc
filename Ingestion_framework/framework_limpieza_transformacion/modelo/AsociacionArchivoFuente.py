class AsociacionArchivoFuente:

    _codigo_fuente_final = -1
    _consecutivo_campo_fuente_final = -1
    _codigo_fuente_cruda = -1
    _consecutivo_campo_fuente_cruda = -1
    _funcion = ''
    
    def __init__(self):
        pass

    def __init__(self, row):
        self.set_codigo_fuente_final(row.codigo_fuente_final)
        self.set_consecutivo_campo_fuente_final(row.consecutivo_campo_fuente_final)
        self.set_codigo_fuente_cruda(row.codigo_fuente_cruda)
        self.set_consecutivo_campo_fuente_cruda(row.consecutivo_campo_fuente_cruda)
        self.set_funcion(row.funcion)

    def set_codigo_fuente_final(self, codigo_fuente_final):
        self._codigo_fuente_final = codigo_fuente_final

    def set_consecutivo_campo_fuente_final(self, consecutivo_campo_fuente_final):
        self._consecutivo_campo_fuente_final = consecutivo_campo_fuente_final

    def set_codigo_fuente_cruda(self, codigo_fuente_cruda):
        self._codigo_fuente_cruda = codigo_fuente_cruda

    def set_consecutivo_campo_fuente_cruda(self, consecutivo_campo_fuente_cruda):
        self._consecutivo_campo_fuente_cruda = consecutivo_campo_fuente_cruda

    def set_funcion(self, funcion):
        self._funcion = funcion

    def get_codigo_fuente_final(self):
        return self._codigo_fuente_final 

    def get_consecutivo_campo_fuente_final(self):
        return self._consecutivo_campo_fuente_final 

    def get_codigo_fuente_cruda(self):
        return self._codigo_fuente_cruda 

    def get_consecutivo_campo_fuente_cruda(self):
        return self._consecutivo_campo_fuente_cruda 

    def get_funcion(self):
        return self._funcion 