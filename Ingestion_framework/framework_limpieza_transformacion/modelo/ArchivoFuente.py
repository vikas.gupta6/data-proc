class ArchivoFuente:

    _codigo_fuente_final = -1
    _codigo_fuente_cruda = -1
    _activo = False
    _obligatorio = False
    _nombre_archivo_origen = ''
    _nombre_archivo_temporal = ''
    _delimitador = ''
    _formato_fecha = ''

    def __init__(self):
        pass

    def __init__(self, row):
        self.set_codigo_fuente_final(row.codigo_fuente_final)
        self.set_codigo_fuente_cruda(row.codigo_fuente_cruda)
        self.set_activo(row.activo)
        self.set_obligatorio(row.obligatorio)
        self.set_nombre_archivo_origen(row.nombre_archivo_origen)
        self.set_nombre_archivo_temporal(row.nombre_archivo_temporal)
        self.set_delimitador(row.delimitador)
        self.set_formato_fecha(row.formato_fecha)

    def set_codigo_fuente_final(self, codigo_fuente_final):
        self._codigo_fuente_final = codigo_fuente_final

    def set_codigo_fuente_cruda(self, codigo_fuente_cruda):
        self._codigo_fuente_cruda = codigo_fuente_cruda

    def set_activo(self, activo):
        self._activo = activo

    def set_obligatorio(self, obligatorio):
        self._obligatorio = obligatorio

    def set_nombre_archivo_origen(self, nombre_archivo_origen):
        self._nombre_archivo_origen = nombre_archivo_origen

    def set_nombre_archivo_temporal(self, nombre_archivo_temporal):
        self._nombre_archivo_temporal = nombre_archivo_temporal

    def set_formato_fecha(self, formato_fecha):
        self._formato_fecha = formato_fecha
    
    def set_delimitador(self, delimitador):
        self._delimitador = delimitador

    def get_codigo_fuente_final(self):
        return self._codigo_fuente_final

    def get_codigo_fuente_cruda(self):
        return self._codigo_fuente_cruda 

    def get_activo(self):
        return self._activo 

    def get_obligatorio(self):
        return self._obligatorio 

    def get_nombre_archivo_origen(self):
        return self._nombre_archivo_origen 

    def get_nombre_archivo_temporal(self):
        return self._nombre_archivo_temporal
        
    def get_formato_fecha(self):
        return self._formato_fecha
    
    def get_delimitador(self):
        return self._delimitador