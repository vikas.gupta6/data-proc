class ParametroTransformacion:

    _codigo_fuente_final = -1
    _codigo_fuente_cruda = -1
    _etapa = ''
    _orden = ''
    _parametro = ''
    _valor = ''

    def __init__(self):
        pass

    def __init__(self, row):
        self.set_codigo_fuente_final(row.codigo_fuente_final)
        self.set_codigo_fuente_cruda(row.codigo_fuente_cruda)
        self.set_etapa(row.etapa)
        self.set_orden(row.orden)
        self.set_parametro(row.parametro)
        self.set_valor(row.valor)

    def set_codigo_fuente_final(self, codigo_fuente_final): 
        self._codigo_fuente_final = codigo_fuente_final

    def set_codigo_fuente_cruda(self, codigo_fuente_cruda): 
        self._codigo_fuente_cruda = codigo_fuente_cruda

    def set_etapa(self, etapa): 
        self._etapa = etapa

    def set_orden(self, orden): 
        self._orden = orden

    def set_parametro(self, parametro): 
        self._parametro = parametro

    def set_valor(self, valor): 
        self._valor = valor
    
    def get_codigo_fuente_final(self):
        return self._codigo_fuente_final
    
    def get_codigo_fuente_cruda(self):
        return self._codigo_fuente_cruda
    
    def get_etapa(self):
        return self._etapa
    
    def get_orden(self):
        return self._orden
    
    def get_parametro(self):
        return self._parametro
    
    def get_valor(self):
        return self._valor
    