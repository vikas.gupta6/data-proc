class TransformacionArchivo:

    _codigo_fuente_final = -1
    _codigo_fuente_cruda = -1
    _etapa = ''
    _orden = ''
    _transformacion = ''

    def __init__(self):
        pass

    def __init__(self, row):
        self.set_codigo_fuente_final(row.codigo_fuente_final)
        self.set_codigo_fuente_cruda(row.codigo_fuente_cruda)
        self.set_etapa(row.etapa)
        self.set_orden(row.orden)
        self.set_transformacion(row.transformacion)

    def set_codigo_fuente_final(self, codigo_fuente_final): 
        self._codigo_fuente_final = codigo_fuente_final

    def set_codigo_fuente_cruda(self, codigo_fuente_cruda): 
        self._codigo_fuente_cruda = codigo_fuente_cruda

    def set_etapa(self, etapa): 
        self._etapa = etapa

    def set_orden(self, orden): 
        self._orden = orden

    def set_transformacion(self, transformacion): 
        self._transformacion = transformacion

    def get_codigo_fuente_final(self):
        return self._codigo_fuente_final
    
    def get_codigo_fuente_cruda(self):
        return self._codigo_fuente_cruda
    
    def get_etapa(self):
        return self._etapa
    
    def get_orden(self):
        return self._orden
    
    def get_transformacion(self):
        return self._transformacion
    