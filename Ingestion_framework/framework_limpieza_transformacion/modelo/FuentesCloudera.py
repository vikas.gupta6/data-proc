class FuentesCloudera:

    _periodo = -1
    _codigo = -1
    _nombre_tabla = ''
    _nombre_archivo = ''
    _descripcion = ''
    _delimitador_campo = ''
    _campos_particion = ''
    _campos_llave_primaria = ''
    _periodicidad = ''
    _activo = ''
    _plataforma = ''
    _aplicativo = ''
    _database_destino = ''
    _sublocation = ''
    _formato = ''
    _header =  ''
    _load_mode = ''
    _apply_functions = ''
    _apply_casts = ''
    _workflow_posterior = ''
    _email_notificar = ''
    _fecha_creacion = ''
    _tipo_ejecucion =  ''
    _periodicidad_ejecucion = ''
    _dias_ejecucion = ''
    _ejecucion_dia_festivo = ''
    _rango_hora_esperada_inicial = ''
    _rango_hora_esperada_final = ''
    _creado_por = ''
    _aplica_certificacion = ''

    def __init__(self):
        pass

    def __init__(self, row):
        self.set_periodo(row.periodo)
        self.set_codigo(row.codigo)
        self.set_nombre_tabla(row.nombre_tabla)
        self.set_nombre_archivo(row.nombre_archivo)
        self.set_descripcion(row.descripcion)
        self.set_delimitador_campo(row.delimitador_campo)
        self.set_campos_particion(row.campos_particion)
        self.set_campos_llave_primaria(row.campos_llave_primaria)
        self.set_periodicidad(row.periodicidad)
        self.set_activo(row.activo)
        self.set_plataforma(row.plataforma)
        self.set_aplicativo(row.aplicativo)
        self.set_database_destino(row.database_destino)
        self.set_sublocation(row.sublocation)
        self.set_formato(row.formato)
        self.set_header(row.header )
        self.set_load_mode(row.load_mode)
        self.set_apply_functions(row.apply_functions)
        self.set_apply_casts(row.apply_casts)
        self.set_workflow_posterior(row.workflow_posterior)
        self.set_email_notificar(row.email_notificar)
        self.set_fecha_creacion(row.fecha_creacion)
        self.set_tipo_ejecucion(row.tipo_ejecucion)
        self.set_periodicidad_ejecucion(row.periodicidad_ejecucion)
        self.set_dias_ejecucion(row.dias_ejecucion)
        self.set_ejecucion_dia_festivo(row.ejecucion_dia_festivo)
        self.set_rango_hora_esperada_inicial(row.rango_hora_esperada_inicial)
        self.set_rango_hora_esperada_final(row.rango_hora_esperada_final)
        self.set_creado_por(row.creado_por)
        #self.set_aplica_certificacion(row.aplica_certificacion)

    def set_periodo(self, periodo): 
        self._periodo = periodo
    
    def set_codigo(self, codigo): 
        self._codigo = codigo
    
    def set_nombre_tabla(self, nombre_tabla): 
        self._nombre_tabla = nombre_tabla
    
    def set_nombre_archivo(self, nombre_archivo): 
        self._nombre_archivo = nombre_archivo
    
    def set_descripcion(self, descripcion): 
        self._descripcion = descripcion
    
    def set_delimitador_campo(self, delimitador_campo): 
        self._delimitador_campo = delimitador_campo
    
    def set_campos_particion(self, campos_particion): 
        self._campos_particion = campos_particion
    
    def set_campos_llave_primaria(self, campos_llave_primaria): 
        self._campos_llave_primaria = campos_llave_primaria
    
    def set_periodicidad(self, periodicidad): 
        self._periodicidad = periodicidad
    
    def set_activo(self, activo): 
        self._activo = activo
    
    def set_plataforma(self, plataforma): 
        self._plataforma = plataforma
    
    def set_aplicativo(self, aplicativo): 
        self._aplicativo = aplicativo
    
    def set_database_destino(self, database_destino): 
        self._database_destino = database_destino
    
    def set_sublocation(self, sublocation): 
        self._sublocation = sublocation
    
    def set_formato(self, formato): 
        self._formato = formato
    
    def set_header(self, header): 
        self._header = header
    
    def set_load_mode(self, load_mode): 
        self._load_mode = load_mode
    
    def set_apply_functions(self, apply_functions): 
        self._apply_functions = apply_functions
    
    def set_apply_casts(self, apply_casts): 
        self._apply_casts = apply_casts
    
    def set_workflow_posterior(self, workflow_posterior): 
        self._workflow_posterior = workflow_posterior
    
    def set_email_notificar(self, email_notificar): 
        self._email_notificar = email_notificar
    
    def set_fecha_creacion(self, fecha_creacion): 
        self._fecha_creacion = fecha_creacion
    
    def set_tipo_ejecucion(self, tipo_ejecucion): 
        self._tipo_ejecucion = tipo_ejecucion
    
    def set_periodicidad_ejecucion(self, periodicidad_ejecucion): 
        self._periodicidad_ejecucion = periodicidad_ejecucion
    
    def set_dias_ejecucion(self, dias_ejecucion): 
        self._dias_ejecucion = dias_ejecucion
    
    def set_ejecucion_dia_festivo(self, ejecucion_dia_festivo): 
        self._ejecucion_dia_festivo = ejecucion_dia_festivo
    
    def set_rango_hora_esperada_inicial(self, rango_hora_esperada_inicial): 
        self._rango_hora_esperada_inicial = rango_hora_esperada_inicial
    
    def set_rango_hora_esperada_final(self, rango_hora_esperada_final): 
        self._rango_hora_esperada_final = rango_hora_esperada_final
    
    def set_creado_por(self, creado_por): 
        self._creado_por = creado_por
    
    def set_aplica_certificacion(self, aplica_certificacion): 
        self._aplica_certificacion = aplica_certificacion
    
    def get_periodo(self):
        return self._periodo
    
    def get_codigo(self):
        return self._codigo
    
    def get_nombre_tabla(self):
        return self._nombre_tabla
    
    def get_nombre_archivo(self):
        return self._nombre_archivo
    
    def get_descripcion(self):
        return self._descripcion
    
    def get_delimitador_campo(self):
        return self._delimitador_campo
    
    def get_campos_particion(self):
        return self._campos_particion
    
    def get_campos_llave_primaria(self):
        return self._campos_llave_primaria
    
    def get_periodicidad(self):
        return self._periodicidad
    
    def get_activo(self):
        return self._activo
    
    def get_plataforma(self):
        return self._plataforma
    
    def get_aplicativo(self):
        return self._aplicativo
    
    def get_database_destino(self):
        return self._database_destino
    
    def get_sublocation(self):
        return self._sublocation
    
    def get_formato(self):
        return self._formato
    
    def get_header(self):
        return self._header
    
    def get_load_mode(self):
        return self._load_mode
    
    def get_apply_functions(self):
        return self._apply_functions
    
    def get_apply_casts(self):
        return self._apply_casts
    
    def get_workflow_posterior(self):
        return self._workflow_posterior
    
    def get_email_notificar(self):
        return self._email_notificar
    
    def get_fecha_creacion(self):
        return self._fecha_creacion
    
    def get_tipo_ejecucion(self):
        return self._tipo_ejecucion
    
    def get_periodicidad_ejecucion(self):
        return self._periodicidad_ejecucion
    
    def get_dias_ejecucion(self):
        return self._dias_ejecucion
    
    def get_ejecucion_dia_festivo(self):
        return self._ejecucion_dia_festivo
    
    def get_rango_hora_esperada_inicial(self):
        return self._rango_hora_esperada_inicial
    
    def get_rango_hora_esperada_final(self):
        return self._rango_hora_esperada_final
    
    def get_creado_por(self): 
        return self._creado_por
    
    def get_aplica_certificacion(self): 
        return self._aplica_certificacion