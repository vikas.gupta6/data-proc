class CamposFuentesCloudera:

    _periodo = -1
    _codigo_fuente = -1
    _consecutivo_campo = -1
    _nombre_campo = ''
    _descripcion = ''
    _campo_particion = ''
    _campo_llave_primaria = ''
    _tipo_campo = ''
    _longitud_campo = ''
    _opcional = -1
    _fx_campo = ''
    _comentarios = ''
    _lista_valores = ''
    
    def __init__(self):
        pass

    def __init__(self, row):
        self.set_periodo(row.periodo)
        self.set_consecutivo_campo(row.consecutivo_campo)
        self.set_nombre_campo(row.nombre_campo)
        self.set_descripcion(row.descripcion)
        self.set_campo_particion(row.campo_particion)
        self.set_campo_llave_primaria(row.campo_llave_primaria)
        self.set_tipo_campo(row.tipo_campo)
        self.set_longitud_campo(row.longitud_campo)
        self.set_opcional(row.opcional)
        self.set_fx_campo(row.fx_campo)
        self.set_comentarios(row.comentarios)
        self.set_lista_valores(row.lista_valores)

    def set_periodo(self, periodo): 
        self._periodo = periodo

    def set_consecutivo_campo(self, consecutivo_campo):
        self._consecutivo_campo = consecutivo_campo

    def set_nombre_campo(self, nombre_campo):
        self._nombre_campo = nombre_campo

    def set_descripcion(self, descripcion):
        self._descripcion = descripcion

    def set_campo_particion(self, campo_particion):
        self._campo_particion = campo_particion

    def set_campo_llave_primaria(self, campo_llave_primaria):
        self._campo_llave_primaria = campo_llave_primaria

    def set_tipo_campo(self, tipo_campo):
        self._tipo_campo = tipo_campo

    def set_longitud_campo(self, longitud_campo):
        self._longitud_campo = longitud_campo

    def set_opcional(self, opcional):
        self._opcional = opcional

    def set_fx_campo(self, fx_campo):
        self.fx_campo = fx_campo

    def set_comentarios(self, comentarios):
        self.comentarios = comentarios

    def set_lista_valores(self, lista_valores):
        self.lista_valores = lista_valores

    def get_periodo(self):
        return self._periodo
    
    def get_consecutivo_campo(self):
        return self._consecutivo_campo
    
    def get_nombre_campo(self):
        return self._nombre_campo
    
    def get_descripcion(self):
        return self._descripcion
    
    def get_campo_particion(self):
        return self._campo_particion
    
    def get_campo_llave_primaria(self):
        return self._campo_llave_primaria
    
    def get_tipo_campo(self):
        return self._tipo_campo
    
    def get_longitud_campo(self):
        return self._longitud_campo
    
    def get_opcional(self):
        return self._opcional

    def get_fx_campo(self):
        return self._fx_campo
    
    def get_comentarios(self):
        return self._comentarios
    
    def get_lista_valores(self):
        return self._lista_valores