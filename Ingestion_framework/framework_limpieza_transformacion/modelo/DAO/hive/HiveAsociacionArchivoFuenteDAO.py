from modelo.DAO.IAsociacionArchivoFuenteDAO import IAsociacionArchivoFuenteDAO
from modelo.AsociacionArchivoFuente import AsociacionArchivoFuente
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class HiveAsociacionArchivoFuenteDAO(IAsociacionArchivoFuenteDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('consecutivo_campo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('consecutivo_campo_fuente_cruda', IntegerType()),
        StructField('funcion', StringType())
        ])
    
    def __init__(self,trace_ejecucion_procesos_manager=None, verbose=False):
        super(HiveAsociacionArchivoFuenteDAO, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager,verbose=verbose)
        if 'nombre_tabla_asociacion_archivo' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_asociacion_archivo'])
    
    def obtener_asociacion_archivos_fuente(self, codigo):
        sql = 'from {tax}.{tabla} select * where codigo_fuente_final = {cod}'.format(tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto(), cod=codigo)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta HIVE - HiveAsociacionArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [AsociacionArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas