from modelo.DAO.IParametroTransformacionDAO import IParametroTransformacionDAO
from modelo.ParametroTransformacion import ParametroTransformacion
#from modelo.TransformacionArchivo import TransformacionArchivo
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class HiveParametroTransformacionDAO(IParametroTransformacionDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('etapa', StringType()),
        StructField('orden', StringType()),
        StructField('parametro', StringType()),
        StructField('valor', StringType())
        ])
    
    def __init__(self, trace_ejecucion_procesos_manager=None,verbose=False):
        super(HiveParametroTransformacionDAO, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_parametro_transformacion' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_parametro_transformacion'])
    
    def obtener_parametros_transformacion(self, codigo_fuente_cruda, codigo_fuente_final, etapa, orden):
        sql = 'from {tax}.{tabla} select * where codigo_fuente_cruda = {cod_fc} and codigo_fuente_final = {cod_ff} and etapa = "{eta}" and orden = {ord}'.format(tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto(), cod_fc=codigo_fuente_cruda, cod_ff=codigo_fuente_final, eta=etapa, ord=orden)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta HIVE - HiveParametroTransformacionDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [ParametroTransformacion(registro) for registro in df_tablas.collect()]
        return arreglo_tablas
