from modelo.DAO.IArchivoFuenteDAO import IArchivoFuenteDAO
from modelo.ArchivoFuente import ArchivoFuente
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class HiveArchivoFuenteDAO(IArchivoFuenteDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('activo', StringType()),
        StructField('obligatorio', StringType()),
        StructField('nombre_archivo_origen', StringType()),
        StructField('nombre_archivo_temporal', StringType()),
        StructField('regex_fecha_nombre', StringType()),
        StructField('delimitador', StringType()),
        StructField('formato_fecha', StringType())
        ])
    
    def __init__(self,trace_ejecucion_procesos_manager=None, verbose=False):
        super(HiveArchivoFuenteDAO, self).__init__(trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_archivo_fuente' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_archivo_fuente'])
    
    def obtener_archivos_fuente(self, codigo):
        sql = 'from {tax}.{tabla} select * where codigo_fuente_final = {cod}'.format(tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto(), cod=codigo)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta HIVE - HiveArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [ArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas

    def obtener_fuentes_archivo(self, archivo):
        sql = 'from {tax}.{tabla} select * where nombre_archivo_origen = "{arc}"'.format(tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto(), arc=archivo)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta HIVE - HiveArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [ArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas

    def obtener_datos(self):
        sql = 'from {tax}.{tabla} select *'.format(tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto())
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta HIVE - HiveArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [ArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas