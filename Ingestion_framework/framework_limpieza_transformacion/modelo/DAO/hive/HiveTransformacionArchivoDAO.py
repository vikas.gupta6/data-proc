from modelo.DAO.ITransformacionArchivoDAO import ITransformacionArchivoDAO
from modelo.TransformacionArchivo import TransformacionArchivo
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class HiveTransformacionArchivoDAO(ITransformacionArchivoDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('etapa', StringType()),
        StructField('orden', StringType()),
        StructField('transformacion', StringType())
        ])
    
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(HiveTransformacionArchivoDAO, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_transformacion_archivo' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_transformacion_archivo'])
    
    def obtener_transformaciones_archivo(self, codigo_fuente_cruda, codigo_fuente_final, etapa):
        sql = 'from {tax}.{tabla} select * where codigo_fuente_cruda = {cod_fc} and codigo_fuente_final = {cod_ff} and etapa = "{eta}" ORDER BY orden'.format(tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto(), cod_fc=codigo_fuente_cruda, cod_ff=codigo_fuente_final, eta=etapa)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta HIVE - HiveTransformacionArchivoDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [TransformacionArchivo(registro) for registro in df_tablas.collect()]
        return arreglo_tablas
