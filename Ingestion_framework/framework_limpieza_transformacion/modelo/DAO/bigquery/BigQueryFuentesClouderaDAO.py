from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.IFuentesClouderaDAO import \
    IFuentesClouderaDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.FuentesCloudera import \
    FuentesCloudera
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class BigQueryFuentesClouderaDAO(IFuentesClouderaDAO):

    _schema = StructType([StructField('periodo', IntegerType()),
        StructField('codigo', IntegerType()),
        StructField('nombre_tabla', StringType()),
        StructField('descripcion', StringType()),
        StructField('delimitador_campos', StringType()),
        StructField('campos_particion', StringType()),
        StructField('campos_llave_primaria', StringType()),
        StructField('periodicidad', StringType()),
        StructField('activo', StringType()),
        StructField('plataforma', StringType()),
        StructField('aplicativo', StringType()),
        StructField('database_destino', StringType()),
        StructField('sublocation', StringType()),
        StructField('formato', StringType()),
        StructField('header', StringType()),
        StructField('load_mode', StringType()),
        StructField('apply_functions', StringType()),
        StructField('workflow_posterior', StringType()),
        StructField('email_notificar', StringType()),
        StructField('fecha_creacion', StringType()),
        StructField('tipo_ejecucion', StringType()),
        StructField('periodicidad_ejecucion', StringType()),
        StructField('dias_ejecucion', StringType()),
        StructField('ejecucion_dia_festivo', StringType()),
        StructField('rango_hora_inicial_esperada', StringType()),
        StructField('rango_hora_final_esperada', StringType())
        ])

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(BigQueryFuentesClouderaDAO, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager,
                                                     verbose=verbose)
        if 'nombre_tabla_fuentes_cloudera' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_fuentes_cloudera'])

    def obtener_informacion_fuente(self, codigo):
        self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["source_bucket"])
        PROJECT_ID = self.get_config()['project_id']
        Database = self.get_config()['taxonomia']
        tabla_name = self.get_nombre_objeto()
        cod = codigo
        df_tablas_fuentes = self.get_sql_context().read.format("bigquery") \
            .option("table", PROJECT_ID + "." + Database + "." + tabla_name) \
            .option('viewsEnabled', 'true') \
            .load()
        df_tablas_fuentes.createOrReplaceTempView("df_tablas_fuentes")
        print("We are in BQFuentesCloudera")
        df_tablas_fuentes.show()

        sql = 'select * from df_tablas_fuentes where codigo = {cod}'.format(cod=codigo)

        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta BigQuery - BigQueryFuentesClouderaDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [FuentesCloudera(registro) for registro in df_tablas.collect()]
        if len(arreglo_tablas) > 1:
            mnj = "Se obtuvo mas de una fuente cloudera relacionada al codigo {0}".format(codigo)
            print(mnj)
            raise InterruptedError(mnj)
        else:
            arreglo_tablas = arreglo_tablas[0]
        return arreglo_tablas

    def obtener_nombre_fuente(self, nombre):
        sql = 'from {tax}.{tabla} select * where nombre_tabla = {nom}'.format(tax=self.get_config()['taxonomia'],
                                                                              tabla=self.get_nombre_objeto(),
                                                                              nom=nombre)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta HIVE - HiveFuentesClouderaDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [FuentesCloudera(registro) for registro in df_tablas.collect()]
        if len(arreglo_tablas) > 1:
            mnj = "Se obtuvo mas de una fuente cloudera relacionada al nombre {0}".format(nombre)
            raise InterruptedError(mnj)
        else:
            arreglo_tablas = arreglo_tablas[0]
        return arreglo_tablas