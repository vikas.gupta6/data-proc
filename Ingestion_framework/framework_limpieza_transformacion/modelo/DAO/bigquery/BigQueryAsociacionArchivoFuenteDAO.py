from modelo.DAO.IAsociacionArchivoFuenteDAO import IAsociacionArchivoFuenteDAO
from modelo.AsociacionArchivoFuente import AsociacionArchivoFuente
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class BigQueryAsociacionArchivoFuenteDAO(IAsociacionArchivoFuenteDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('consecutivo_campo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('consecutivo_campo_fuente_cruda', IntegerType()),
        StructField('funcion', StringType())
        ])
    
    def __init__(self, verbose=False):
        raise NotImplementedError()
    
    def obtener_asociacion_archivos_fuente(self, codigo):
        raise NotImplementedError()