from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.ICamposFuentesClouderaDAO import \
    ICamposFuentesClouderaDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.CamposFuentesCloudera import \
    CamposFuentesCloudera
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class BigQueryCamposFuentesClouderaDAO(ICamposFuentesClouderaDAO):

    _schema = StructType([StructField('periodo', IntegerType()),
        StructField('codigo_fuente', IntegerType()),
        StructField('consecutivo_campo', IntegerType()),
        StructField('nombre_campo', StringType()),
        StructField('descripcion', StringType()),
        StructField('campo_particion', StringType()),
        StructField('campo_llave_primaria', StringType()),
        StructField('tipo_campo', StringType()),
        StructField('longitud_campo', StringType()),
        StructField('opcional_campo', IntegerType()),
        StructField('fx_campo', StringType()),
        StructField('comentarios', StringType()),
        StructField('lista_valor', StringType())
        ])

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(BigQueryCamposFuentesClouderaDAO, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_campos_fuentes_cloudera' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_campos_fuentes_cloudera'])

    def obtener_campos_fuente(self, codigo):
        self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["source_bucket"])
        PROJECT_ID = self.get_config()['project_id']
        Database = self.get_config()['taxonomia']
        tabla_name = self.get_nombre_objeto()
        cod = codigo
        df_tablas_view = self.get_sql_context().read.format("bigquery") \
            .option("table", PROJECT_ID + "." + Database + "." + tabla_name) \
            .option('viewsEnabled', 'true') \
            .load()
        df_tablas_view.createOrReplaceTempView("df_tablas_view")
        print("We are in BQCamposFuentesClouderaDAO")
        df_tablas_view.show()


        sql = 'select * from df_tablas_view where codigo_fuente = {cod} order by consecutivo_campo'.format(cod=codigo)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta BigQuery - BigQueryCamposFuentesClouderaDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [CamposFuentesCloudera(registro) for registro in df_tablas.collect()]
        return arreglo_tablas

