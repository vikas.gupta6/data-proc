from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.ITransformacionArchivoDAO import \
    ITransformacionArchivoDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.TransformacionArchivo import \
    TransformacionArchivo
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class BigQueryTransformacionArchivoDAO(ITransformacionArchivoDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('etapa', StringType()),
        StructField('orden', StringType()),
        StructField('transformacion', StringType())
        ])

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(BigQueryTransformacionArchivoDAO, self).__init__(
            trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_transformacion_archivo' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_transformacion_archivo'])

    def obtener_transformaciones_archivo(self, codigo_fuente_cruda, codigo_fuente_final, etapa):
        self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["source_bucket"])
        df_tablas_archivo = self.get_sql_context().read.format('bigquery') \
            .option('project', self.get_config()['project_id']) \
            .option('dataset', self.get_config()['taxonomia']) \
            .option('table', self.get_nombre_objeto()) \
            .load()

        df_tablas_archivo.createOrReplaceTempView('df_tablas_archivo_view')
        print("This is df_tablas_archivo_view")
        df_tablas_archivo.show()
        # Preparing Query as string
        sql = 'select * from df_tablas_archivo_view  where codigo_fuente_cruda = {cod_fc} and codigo_fuente_final = {cod_ff} and etapa = "{eta}" ORDER BY orden'.format(
            cod_fc=codigo_fuente_cruda,
            cod_ff=codigo_fuente_final, eta=etapa)

        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta BQ - BigQueryTransformacionArchivoDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [TransformacionArchivo(registro) for registro in df_tablas.collect()]
        return arreglo_tablas





