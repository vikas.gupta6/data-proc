from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.IArchivoFuenteDAO import \
    IArchivoFuenteDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.ArchivoFuente import \
    ArchivoFuente
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

from google.cloud import bigquery
import google.auth
import pandas
from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
import base64


credentials,project = google.auth.default()
bqclient = bigquery.Client(credentials=credentials)

class BigQueryArchivoFuenteDAO(IArchivoFuenteDAO):
    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
                          StructField('codigo_fuente_cruda', IntegerType()),
                          StructField('activo', StringType()),
                          StructField('obligatorio', StringType()),
                          StructField('nombre_archivo_origen', StringType()),
                          StructField('nombre_archivo_temporal', StringType()),
                          StructField('regex_fecha_nombre', StringType()),
                          StructField('delimitador', StringType()),
                          StructField('formato_fecha', StringType())
                          ])

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(BigQueryArchivoFuenteDAO, self).__init__(trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_archivo_fuente' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_archivo_fuente'])

    def obtener_archivos_fuente(self, codigo):
        sql = 'from {tax}.{tabla} select * where codigo_fuente_final = {cod}'.format(tax=self.get_config()['taxonomia'],
                                                                                     tabla=self.get_nombre_objeto(),
                                                                                     cod=codigo)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta HIVE - BigQueryArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [ArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas

    def obtener_fuentes_archivo(self, archivo):
        sql = 'from {tax}.{tabla} select * where nombre_archivo_origen = "{arc}"'.format(
            tax=self.get_config()['taxonomia'], tabla=self.get_nombre_objeto(), arc=archivo)
        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta HIVE - BigQueryArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        arreglo_tablas = [ArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas

    def obtener_datos(self):
        self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["source_bucket"])
        PROJECT_ID = self.get_config()['project_id']
        Database = self.get_config()['taxonomia']
        tabla_name = self.get_nombre_objeto()
        df_tablas = self.get_sql_context().read.format("bigquery") \
            .option("table", PROJECT_ID + "." + Database + "." + tabla_name) \
            .option('viewsEnabled', 'true') \
            .load()

        sql = 'select * from '+PROJECT_ID+'.'+Database+'.'+tabla_name

        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log='Ejecutando consulta BigQuery - BigQueryArchivoFuenteDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        arreglo_tablas = [ArchivoFuente(registro) for registro in df_tablas.collect()]
        return arreglo_tablas



