from Ingestion_framework.framework_limpieza_transformacion.modelo.DAO.IParametroTransformacionDAO import \
    IParametroTransformacionDAO
from Ingestion_framework.framework_limpieza_transformacion.modelo.ParametroTransformacion import \
    ParametroTransformacion
# from Ingestion_framework.framework_limpieza_transformacion.modelo.TransformacionArchivo import \
#     TransformacionArchivo
from pyspark.sql.types import IntegerType, DateType, StringType, StructField, StructType, TimestampType

class BigQueryParametroTransformacionDAO(IParametroTransformacionDAO):

    _schema = StructType([StructField('codigo_fuente_final', IntegerType()),
        StructField('codigo_fuente_cruda', IntegerType()),
        StructField('etapa', StringType()),
        StructField('orden', StringType()),
        StructField('parametro', StringType()),
        StructField('valor', StringType())
        ])

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(BigQueryParametroTransformacionDAO, self).__init__(
            trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        if 'nombre_tabla_parametro_transformacion' in self.get_config().keys():
            self.set_nombre_objeto(self.get_config()['nombre_tabla_parametro_transformacion'])

    def obtener_parametros_transformacion(self, codigo_fuente_cruda, codigo_fuente_final, etapa, orden):
        # self.get_sql_context().setConf("temporaryGcsBucket", self.get_config()["source_bucket"])
        PROJECT_ID = self.get_config()['project_id']
        tax = self.get_config()['taxonomia']
        tabla = self.get_nombre_objeto()

        df_tablas_view_transformation = self.get_sql_context().read.format("bigquery") \
            .option("table", PROJECT_ID + "." + tax + "." + tabla) \
            .option('viewsEnabled', 'true') \
            .load()

        df_tablas_view_transformation.createOrReplaceTempView("df_tablas_view_transformation")
        print("This is df_tablas of BigQueryParametroTransformacionDAO")
        df_tablas_view_transformation.show()

        sql = 'select * from df_tablas_view_transformation where codigo_fuente_cruda = {cod_fc} and codigo_fuente_final = {cod_ff} and etapa = "{eta}" and orden = {ord}'.format(cod_fc=codigo_fuente_cruda,
            cod_ff=codigo_fuente_final, eta=etapa, ord=orden)

        if self.get_trace_ejecucion_procesos_manager() is not None and self.get_trace_ejecucion_procesos_manager().is_verbose_dao():
            log = 'Ejecutando consulta Bigquery - BigQueryParametroTransformacionDAO'
            self.get_trace_ejecucion_procesos_manager().insert_trace_ejecucion_proceso(log, codigo_ejecutado=sql)
        df_tablas = self.get_sql_context().sql(sql)
        print("BigQueryParametroTransformacionDAO df_tablas {}".format(df_tablas))
        arreglo_tablas = [ParametroTransformacion(registro) for registro in df_tablas.collect()]
        print("BigQueryParametroTransformacionDAO arreglo_tablas {}".format(arreglo_tablas))
        return arreglo_tablas