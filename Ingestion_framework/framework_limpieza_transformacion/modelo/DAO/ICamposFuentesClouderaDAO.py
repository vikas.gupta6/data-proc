from abc import ABCMeta, abstractmethod
from Ingestion_framework.utilitarios.Utiles import \
    Utiles

class ICamposFuentesClouderaDAO:

    __metaclass__ = ABCMeta

    _nombre_objeto = ''
    _verbose = False
    _spark_context = None
    _sql_context = None
    _config = None
    _trace_ejecucion_procesos_manager = None

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        self.set_trace_ejecucion_procesos_manager(trace_ejecucion_procesos_manager)
        self.set_verbose(verbose)
        self._spark_context, self._sql_context = Utiles().get_spark_session()
        self.set_config(Utiles().load_config('gs://testing-ingestion/config_files/config.json'))

    @abstractmethod
    def obtener_campos_fuente(self, codigo):
        pass

    def get_nombre_objeto(self):
        return self._nombre_objeto
    
    def set_nombre_objeto(self, nombre_objeto):
        self._nombre_objeto = nombre_objeto
    
    def get_schema(self):
        return self._schema
    
    def get_sql_context(self):
        return self._sql_context
    
    def get_spark_context(self):
        return self._spark_context
    
    def get_config(self):
        return self._config
    
    def get_trace_ejecucion_procesos_manager(self):
        return self._trace_ejecucion_procesos_manager

    def is_verbose(self):
        return self._verbose
    
    def set_sql_context(self, sql_context):
        self._sql_context = sql_context
    
    def set_spark_content(self, spark_context):
        self._spark_context = spark_context
    
    def set_verbose(self, verbose):
        self._verbose = verbose
    
    def set_schema(self, schema):
        self._schema = schema
    
    def set_config(self, config):
        self._config = config
    
    def set_trace_ejecucion_procesos_manager(self, trace_ejecucion_procesos_manager):
        self._trace_ejecucion_procesos_manager = trace_ejecucion_procesos_manager