# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy

from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes
import pyspark.sql.functions as F

class EliminarCamposVaciosStrategy(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(EliminarCamposVaciosStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de eliminar tanto filas como columnas que estan complentamente vacias.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformacion.
        '''
        # Remover las filas que son completamente vacias
        try:
            fuente = fuente.dropna("all")
            return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex) 