# -*- coding: UTF-8 -*-
from time import strftime, strptime
from ITransformacionStrategy import ITransformacionStrategy
from datetime import datetime
from Utiles import Utiles
import Constantes

class ReplicarArchivoDiaNoHabil(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ReplicarArchivoDiaNoHabil, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
    
    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de cambiar los saltos de línea de windows o mac a unix.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformacion.
        '''

        dias_no_habiles = parametro['DIAS_NO_HABILES']('DIAS_NO_HABILES')
        dias_no_habiles = [dia.get_lv_valor() for dia in dias_no_habiles]

        nombre_archivo = parametro['NOMBRE_ARCHIVO']
        ruta_archivo = parametro['RUTA_ARCHIVO']
        ruta_archivo_destino = ruta_archivo.replace('.Processing/', '')

        periodo_actual = parametro['PERIODO_EJECUCION']
        periodo_siguiente = strptime(periodo_actual, '%Y%m%d')
        
        # Sumar un dia a la fecha de ejecucion
        periodo_siguiente = periodo_siguiente.timedelta(days=1)

        periodo_siguiente = strftime('%Y%m%d', periodo_siguiente)

        if periodo_siguiente in dias_no_habiles:
            Utiles().copiar_archivo(nombre_archivo, ruta_archivo, ruta_archivo_destino, nombre_archivo.replace(periodo_actual,periodo_siguiente))
        
        return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA