# -*- coding: UTF-8 -*-
from decimal import InvalidOperation
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.functions import udf, struct
from pyspark.sql.types import StringType
import re

class FiltrarRegistrosStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(FiltrarRegistrosStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar un string en una posicion fija en todas las filas de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''

        PARAMETROS_OBLIGATORIOS = ['CONDICION']

        for par in PARAMETROS_OBLIGATORIOS:
            if par not in parametro.keys():
                return fuente, False, 'El parametro {parametro} es obligatorio'.format(parametro=par)

        expresion_columna_solicitada = r'({(COLUMNA([0-9]+))})'
        
        regex_columna_solicitada = re.compile(expresion_columna_solicitada)

        condicion = parametro['CONDICION']

        for columna in regex_columna_solicitada.findall(condicion):
            numero_columna = int(columna[2])-1
            condicion = condicion.replace(columna[0], fuente.columns[numero_columna])

        fuente = fuente.where(condicion)

        return fuente, True, "Tansformacion aplicada correctamente"
