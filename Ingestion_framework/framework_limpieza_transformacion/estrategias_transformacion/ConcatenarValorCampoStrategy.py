# -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

class ConcatenarValorCampoStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ConcatenarValorCampoStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar un string en una posicion fija en todas las filas de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''

        PARAMETROS_OBLIGATORIOS = ['COLUMNAS', 'VALOR', 'POSICION']
        POSICIONES_VALIDAS = ['INICIO', 'FINAL']

        for par in PARAMETROS_OBLIGATORIOS:
            if par not in parametro.keys():
                return fuente, False, 'El parametro {parametro} es obligatorio'.format(parametro=par)
        
        if parametro['POSICION'].upper() not in POSICIONES_VALIDAS:
            return fuente, False, 'El parametro POSICION es invalido. debe ser uno de los siguientes valores: {valores}'.format(valores=POSICIONES_VALIDAS)

        columnas = [int(columna)-1 for columna in parametro['COLUMNAS'].split(',')]
        valor = parametro['VALOR']
        posicion = parametro['POSICION'].upper()

        def concatenar_valor_campo(campo):
            return '{campo}{valor}'.format(campo, valor) if posicion == 'FINAL' else '{valor}{campo}'.format(valor, campo) 
        
        udf_concatenar_valor_campo = udf(concatenar_valor_campo)

        for columna in columnas:
            fuente = fuente.withColumn(fuente.columns[columna], udf_concatenar_valor_campo(fuente.columns[columna]))

        return fuente, True, "Tansformacion aplicada correctamente"
