# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

class CambioCodificacionStrategy(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(CambioCodificacionStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de cambiar la codificación de los datos.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        try:
            parametro = parametro['NUEVA_CODIFICACION']
            f_decoder = udf(lambda x: x.encode(parametro).decode(parametro) if x else None, StringType())
            for col in fuente.columns:
                fuente = fuente.withColumn(col, f_decoder(col))
            return fuente, True, "Transformacion aplicada correctamente"
        except LookupError:
            return fuente, False, "La codificacion {0} no existe".format(parametro)
