# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import substring
from pyspark.sql.types import StringType
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes

class FijoASeparadorStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(FijoASeparadorStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''        

        
        if 'LONGITUDES' not in parametro.keys():
            raise ValueError('Se esperaba el parametro POSICIONES en diccionario de entrada')
        try:
            posicion_actual = 1
            for i, item in enumerate(parametro['LONGITUDES'].split(' ')):
                fuente = fuente.withColumn('C_{}'.format(i+1), substring(fuente.columns[0], posicion_actual, int(item)))
                posicion_actual = posicion_actual + int(item)
            fuente = fuente.drop(fuente.columns[0])
        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex) 
        return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA