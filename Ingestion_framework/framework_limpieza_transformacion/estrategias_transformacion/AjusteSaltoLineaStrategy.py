# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes

class AjusteSaltoLineaStrategy(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(AjusteSaltoLineaStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
    
    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de cambiar los saltos de línea de windows o mac a unix.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformacion.
        '''

        

        parametro = parametro['MARCADOR']
        rw = '\r\n'  # Win
        rm = '\r'    # Mac
        ru = '\n'    # Unix
        if parametro.lower() == 'unix':
            r1 = rw
            r2 = rm
            rp = ru
            return fuente.replace(r1, rp).replace(r2, rp), True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        elif parametro.lower() == 'win':
            r1 = ru
            r2 = rm
            rp = rw
            return fuente.replace(r1, rp).replace(r2, rp), True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        elif parametro.lower() == 'mac':
            r1 = rw
            r2 = ru
            rp = rm
            return fuente.replace(r1, rp).replace(r2, rp), True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        else:
            return fuente, False, "El parámetro no es ninguno de los permitidos (unix, win, mac)"
