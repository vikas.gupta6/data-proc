# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes

from pyspark.sql.functions import concat, col

class ConcatenarColumnasStrategy(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ConcatenarColumnasStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de concatenar 2 columnas y ubicar el resultado en una posicion especificada.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformacion.
        '''
        index_cols = parametro["COLUMNAS"].split(",")
        index_cols = [int(i) for i in index_cols]
        total_cols = len(index_cols)
        if total_cols % len(index_cols[0::3]) == 0:
            cantidad = 3
            inicio = 0
            final = inicio + cantidad - 1
            posiciones = index_cols[2::3]
            posiciones.sort()
            posiciones = posiciones[::-1]
            num_ciclos = len(posiciones)
            cols = fuente.columns
            orden = list(cols)
            for i in posiciones:
                orden.insert(i-1, 'col{0}'.format(i))
            for i in range(num_ciclos):
                fuente = fuente.select("*", concat(col(cols[index_cols[inicio]-1]), col(cols[index_cols[final-1]-1])).alias("col{0}".format(index_cols[final])))
                inicio += cantidad
                final += cantidad
            return fuente.select(orden), True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        else:
            return fuente, False, "La transformacion no se pudo aplicar"