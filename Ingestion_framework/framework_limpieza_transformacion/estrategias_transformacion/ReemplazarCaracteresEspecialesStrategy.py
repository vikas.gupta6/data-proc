# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql import functions as f

class ReemplazarCaracteresEspecialesStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ReemplazarCaracteresEspecialesStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta Transformacion se encarga de reemplazar los caracteres especiales.
        Parametros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        Retorna:
            fuente: fuente transformada.
            Bool: Valor que indica si se ejecutó correctamente o no la transformacion  (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.

        '''
        # Valores por defecto
        if parametro['CARACTERES_BUSQUEDA'] == '' and parametro['CARACTERES_REEMPLAZO'] == '':
            parametro['CARACTERES_BUSQUEDA'], parametro['CARACTERES_REEMPLAZO'] = 'áéíóú,', 'aeiou.'
        try:
            print("This is reamplazar")
            print(fuente.columns,parametro['CARACTERES_BUSQUEDA'], parametro['CARACTERES_REEMPLAZO'])
            for col in fuente.columns:
                fuente = fuente.withColumn(col, f.translate(col, parametro['CARACTERES_BUSQUEDA'], parametro['CARACTERES_REEMPLAZO']))
            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)
