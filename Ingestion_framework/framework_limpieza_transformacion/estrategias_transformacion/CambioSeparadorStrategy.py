# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

class CambioSeparadorStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(CambioSeparadorStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de cambiar el separador de los datos.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        parametros = ['SEPARADOR_ORIGEN', 'SEPARADOR_DESTINO']
        if parametros[0] in parametro.keys():
            if parametros[1] in parametro.keys():
                f_cambio_separador = udf(lambda x: x.replace(parametro['SEPARADOR_ORIGEN'], parametro['SEPARADOR_DESTINO']), StringType())
                for col in fuente.columns:
                    fuente = fuente.withColumn(col, f_cambio_separador(col))
                return fuente, True, "Tansformacion aplicada correctamente"
            else:
                return fuente, False, "No se encontro el caracter de reemplazo"
        else:
            return fuente, False, "No se encontro el caracter de busqueda"
