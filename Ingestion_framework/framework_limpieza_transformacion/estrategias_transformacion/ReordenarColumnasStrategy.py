# # -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy

class ReordenarColumnasStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ReordenarColumnasStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self,fuente, parametro):
        '''
        Esta transformacion se encarga de saltear las columnas indicadas en el parametro
        parametros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
            str: Mensaje que indica si se ejecuto correctamente o no la transformacion.
        '''
        try:

            PARAMETROS_OBLIGATORIOS = ["POSICIONES"]

            for par in PARAMETROS_OBLIGATORIOS:
                if par not in parametro.keys():
                    return fuente, False, 'El parametro {0} es obligatorio'.format(par)
            
            pos = [columna for columna in parametro['POSICIONES'].split(',')]
            columnas = list()
            
            for c in pos:
                if '-' in c:
                    columnas.extend(range(int(c.split('-')[0])-1, int(c.split('-')[1])))
                else:
                    columnas.append(int(c)-1)

            if len(fuente.columns) != len(columnas):
                return fuente, False, "El parametro POSICIONES es invalido. Debe ser del mismo numero de columnas que el dataframe original"

            names = []
            for p in columnas:
                names.append(fuente.columns[p])
            fuente = fuente.select(names)

            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {0}".format(ex)
































# input = [1,2,3,4,5,6,7,8,9,10]


# pos =["5-6","1-4","10","7-9"]

# "5-6"
# columna_final.extend(range(int(col.split('-')[0])-1, int(col.split('-')[1])))
# columna_final.extend(4,6)
# "1-4"
# columna_final.extend(range(0, 4))
# columna_final.extend(4,5,0,1,2,3)
# "10"
# columna_final.append(10-9)
# columna_final.append(4,5,0,1,2,3,9)
# "7-9"
# columna_final.extend(range(6, 9))
# columna_final.extend(4,5,0,1,2,3,9,6,7,8)

# for i in columna_final:
# data_final.append(input[i])

# data_final = [5,6,1,2,3,4,10,7,8,9]



# columnas = [0,1,2,3,4,5,6,7,9]

# range(int(2,8))




# [5,6,1,2,3,4,7,8,9,10]


# l =[[5,6],[1,2,3,4],[10],[7,8,9]]

# lista_final = []
# for lista in l:
# lista_final.extend(lista)



# parametro =


# output = [5,6,1,2,3,4,10,7,8,9]