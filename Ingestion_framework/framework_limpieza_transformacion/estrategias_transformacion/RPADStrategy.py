# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import rpad

class RPADStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(RPADStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de completar una columna con un caracter dado a la derecha
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        try:
            columna = parametro['COLUMNAS']
            longitud = parametro['LONGITUD']
            caracter = parametro['CARACTER']
            fuente = fuente.withColumn(fuente.columns[columna], rpad(fuente.columns[columna], longitud, caracter))
            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)
