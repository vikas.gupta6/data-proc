# -*- coding: UTF-8 -*-
from decimal import InvalidOperation
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.functions import udf, struct
from pyspark.sql.types import StringType
import re

class ConcatenarValorCampoCondicionalStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ConcatenarValorCampoCondicionalStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar un string en una posicion fija en todas las filas de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''

        PARAMETROS_OBLIGATORIOS = ['COLUMNA']

        for par in PARAMETROS_OBLIGATORIOS:
            if par not in parametro.keys():
                return fuente, False, 'El parametro {parametro} es obligatorio'.format(par)

        columna = int(parametro['COLUMNA'])-1
        
        #Condiciones deben tener la forma CONDICION_X donde X es un numero
        condiciones = {key:value for (key,value) in parametro.items() if 'CONDICION' in key.upper()}
        #Default, en caso de que exista
        default = parametro['ELSE'] if 'ELSE' in parametro else None
        #Condiciones deben tener la forma VALOR_X donde X es un numero y 
        #hace referencia al valor a colocar en caso de que se cumpla la condicion
        valores = {key:value for (key,value) in parametro.items() if 'VALOR' in key.upper()}
        
        if len(condiciones) == 0 or len(condiciones) != len(valores):
            return fuente, False, 'Las condiciones se encuentran mal parametrizadas'
        
        expresion_funcion = r'(([A-Za-z]+)\((.*)\))'
        expresion_columna_solicitada = r'({(COLUMNA[0-9]+)})'
        
        regex_validar_funcion = re.compile(expresion_funcion)
        regex_columna_solicitada = re.compile(expresion_columna_solicitada)

        funciones_soportadas = {
            'LEN': (lambda x: len(x)),
            'SUBSTR': (lambda valor, inicio, caracteres: valor[inicio:inicio+caracteres])
        }

        def concatenar_valor_campo(campo):
            for i in range(len(condiciones)):
                condicion = condiciones['CONDICION_{}'.format(i+1)]
                valor = valores['VALOR_{}'.format(i+1)]

                resultados_validaciones = []
                operacion_logica = None
                if '&&' in condicion: #validacion AND
                    operacion_logica = '&&'
                elif '||' in condicion: #validacion OR
                    operacion_logica = '||'
                
                if operacion_logica:
                    for c in condicion.split(operacion_logica):
                        validar_expresion(campo, resultados_validaciones, c.strip())
                else:
                    validar_expresion(campo, resultados_validaciones, condicion)
                
                if operacion_logica is None or operacion_logica == '&&':
                    if all(resultados_validaciones):
                        valor = obtener_valor(campo, valor)
                        return valor
                elif any(resultados_validaciones):
                    valor = obtener_valor(campo, valor)
                    return valor
            if default:
                valor = obtener_valor(campo, default)
                return valor
            return campo[columna]

        def validar_expresion(campo, resultados_validaciones, c):
            comparacion = None
            if '==' in c:
                comparacion = '=='
            elif '>' in c:
                comparacion = '>'
            elif '<' in c:
                comparacion = '<'
            else:
                raise InvalidOperation('Comparacion de validacion no soportada - {0}'.format(c))
            componentes = c.split(comparacion)
            # obtener valor izquierda
            valor1 = obtener_valor(campo, componentes[0])
            # obtener valor derecha
            valor2 = obtener_valor(campo, componentes[1])
            resultado_validacion = None
            if comparacion == '==':
                resultado_validacion = valor1==valor2
            elif comparacion == '>':
                resultado_validacion = float(valor1)>float(valor2)
            elif comparacion == '<':
                resultado_validacion = float(valor1)<float(valor2)
            resultados_validaciones.append(resultado_validacion)

        def obtener_valor(campo, componente):
            valor = None
            columnas_solicitadas = regex_columna_solicitada.findall(componente)
            if columnas_solicitadas:
                for columna in columnas_solicitadas:
                    columna_solicitada = str(columna[0])
                    numero_campo = int(columna[1].upper().strip().replace('COLUMNA', '')) -1
                    componente = componente.replace(columna_solicitada, str(campo[numero_campo]))
            funcion_aplicar = regex_validar_funcion.search(componente)
            if funcion_aplicar: # Es una funcion
                fx = funcion_aplicar.group(2).upper()
                parametros = funcion_aplicar.group(3).split(',')
                for i, parametro in enumerate(parametros):
                    if 'COLUMNA' in parametro.upper():
                        parametros[i] = campo[int(parametro.upper().replace('COLUMNA', ''))-1]
                    else:
                        parametros[i] = parametro.strip()
                try:
                    valor = str(componente).replace(funcion_aplicar.group(1), str(funciones_soportadas[fx](*parametros))) #re.sub(expresion_funcion, str(funciones_soportadas[fx](*parametros)), componente)
                except KeyError:
                    raise InvalidOperation('Funcion no soportada')
            if not valor:
                valor = componente.strip()
            return valor
        
        udf_concatenar_valor_campo = udf(concatenar_valor_campo)

        fuente = fuente.withColumn(fuente.columns[columna], udf_concatenar_valor_campo(struct([fuente[x] for x in fuente.columns])))

        return fuente, True, "Tansformacion aplicada correctamente"
