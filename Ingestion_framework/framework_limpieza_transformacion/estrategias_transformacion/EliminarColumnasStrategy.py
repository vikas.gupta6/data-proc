# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import lit
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes

class EliminarColumnasStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(EliminarColumnasStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de eliminar columnas en el dataframe en las posiciones indicadas.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        try:
            pos = parametro['POSICIONES']
            pos = pos.split(',')
            columnas = list()
            for col in pos:
                if '-' in col:
                    columnas.extend(range(int(col.split('-')[0]), int(col.split('-')[1])))
                else:
                    columnas.append(int(col))
            pos.sort(reverse=True)
            for p in pos:
                fuente = fuente.drop(fuente.columns[p])
            return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex) 
