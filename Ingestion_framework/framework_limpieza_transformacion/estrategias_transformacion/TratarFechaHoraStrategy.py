# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from datetime import datetime
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes

class TratarFechaHoraStrategy(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(TratarFechaHoraStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        formatos_busqueda = {
            "FECHA":
            [
                "%d%m%Y",
                "%d/%m/%Y",
                "%d-%m-%Y",
                "%Y%m%d",
                "%Y/%m/%d",
                "%Y-%m-%d",
                "%-d%m%Y",
                "%-d/%m/%Y",
                "%-d/%-m/%Y",
                "%d-%b-%y"
            ],

            "FECHA_HORA":
            [
                "%d/%m/%Y %H:%M:%S",
                "%d%b%Y:%H:%M:%S",
            ],

            "HORA":
            [
                "%I%M%S%p",
                "%H:%M:%S",
                "%I:%M:%S%p"
            ]
        }

        formatos_aplicar = {
            "FECHA": "%Y%m%d",
            "FECHA_HORA": "%Y%m%d%H%M%S",
            "HORA": "%H%M%S"
            }

        def tratar_fecha(val, form_busq, form_apl, formato_origen=None):
            if not formato_origen:
                for fmt in form_busq:
                    try:
                        tmp = datetime.strptime(val, fmt)
                        nuevo_formato = datetime.strftime(tmp, form_apl)
                        return nuevo_formato
                    except ValueError:
                        pass
            else:
                try:
                    tmp = datetime.strptime(val, formato_origen)
                    nuevo_formato = datetime.strftime(tmp, form_apl)
                    return nuevo_formato
                except ValueError:
                    raise ValueError('No fue posible tratar la fecha {} desde el formato {} al {}'.format(val, formato_origen, form_apl))
            return val

        formato = parametro["FORMATO"]
        cols_index = parametro["COLUMNAS"]

        formato_origen = parametro['FORMATO_ORIGEN'] if 'FORMATO_ORIGEN' in parametro else None

        try:
            cols_index = cols_index.split('-')
            cols = [fuente.columns[int(col)-1] for col in cols_index]
            f_tratar_fecha = udf(lambda x: tratar_fecha(x, formatos_busqueda[formato], formatos_aplicar[formato], formato_origen))
            for col in cols:
                fuente = fuente.withColumn(col, f_tratar_fecha(col))
            return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex)