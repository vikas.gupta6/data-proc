# -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

class FijoASeparadorStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(FijoASeparadorStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar un string en una posicion fija en todas las filas de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''        
        def insertar_separador(cadena, posiciones, separador):
            tmp_str = cadena
            for p in posiciones:
                tmp_str = tmp_str[:p] + separador + tmp_str[p:]
            return tmp_str
        
        if 'POSICIONES' in parametro.keys():
            pos = parametro['POSICIONES']
            if 'CARACTER' in parametro.keys():
                sep = parametro['CARACTER']
                tmpp = [i for i in range(len(pos))]
                posi = [sum(pos[:len(pos)-i]) for i in tmpp]
                f_agregar_char = udf(lambda x: insertar_separador(x, posi, sep), StringType())
                return fuente.withColumn('value', f_agregar_char('value')), True, "Tansformacion aplicada correctamente"
            else:
                return fuente, False, "El parametro CARACTER no se encontro"
        else:
            return fuente, False, "El parametro POSICION no se encontro"