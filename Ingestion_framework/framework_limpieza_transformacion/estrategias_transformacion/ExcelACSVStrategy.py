# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
import os
import pyspark
from pyspark.sql.functions import col, udf
from pyspark.sql.types import StringType
import unicodedata
from pyspark import SparkFiles
import pandas
from datetime import datetime
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes
os.system("python3 -m pip install xlrd")
import xlrd
from Ingestion_framework.utilitarios.Utiles import \
    Utiles

from pyspark.sql.types import StringType

class ExcelACSVStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ExcelACSVStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        file_name = 'gs://testing-ingestion/config_files/config.json'
        self.config = Utiles().load_config(file_name)
        self.bucket_name = self.config['source_bucket']

    def aplicar_transformacion(self, fuente, parametro):
        '''
        
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''        
        try:

            ruta_archivo = parametro['RUTA_ARCHIVO']
            nombre_archivo = parametro['NOMBRE_ARCHIVO']
            nombre_hoja = parametro['NOMBRE_HOJA']
            encoding = 'utf-8' if 'ENCODING' not in parametro else parametro['ENCODING']
            formato_fecha = '%G%m%d' if 'FORMATO_FECHA' not in parametro else parametro['FORMATO_FECHA']

            self._spark_context.addFile('gs://' + str(self.bucket_name+'/'+ruta_archivo+nombre_archivo))

            workbook = xlrd.open_workbook(SparkFiles.get(nombre_archivo))
            
            if type(fuente) != pyspark.sql.DataFrame:
                fuente = pandas.read_excel(workbook, nombre_hoja, engine='xlrd', header=None)


            def simplify(text):
                if type(text) in (int, float):
                    return '{}'.format(text)
                if type(text) in (datetime,):
                    return text.strftime(formato_fecha)
                try:
                    text = text.encode(encoding)
                    text = unicode(text, encoding)
                except NameError:
                    pass
                text = unicodedata.normalize('NFD', text).encode(encoding, 'ignore').decode(encoding)
                return text

            for col in fuente.columns:
                fuente[col] = fuente[col].apply(simplify)
            
            fuente = self._sql_context.createDataFrame(fuente)

        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex) 
        return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA
    