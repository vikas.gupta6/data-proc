# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import regexp_replace

class QuitarCerosIzquierdaStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(QuitarCerosIzquierdaStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de quitar los ceros a la izquierda que sobran.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        try:
            columnas = [int(columna) - 1 for columna in parametro['COLUMNAS'].split(',')]
            for col in columnas:
                expresion = '([0]*)([0-9].*)'
                fuente = fuente.withColumn(fuente.columns[col], regexp_replace(fuente.columns[col], expresion, '$2'))
            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)
