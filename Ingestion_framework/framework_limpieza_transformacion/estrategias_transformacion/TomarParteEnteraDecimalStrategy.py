# # -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.functions import floor,regexp_replace

class TomarParteEnteraDecimalStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(TomarParteEnteraDecimalStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self,fuente, parametro):
            '''
            Esta transformacion se encarga de dejar la parte entera o decimal de un numero, dependiendo de un parametro
            parametros:
                fuente: Datos en formato Dataframe.
                parametro: Diccionario.

            retorna:
                fuente: fuente transformada.
                Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
                str: Mensaje que indica si se ejecuto correctamente o no la transformacion.
            '''
            try:
                PARAMETROS_OBLIGATORIOS = ['COLUMNAS', 'TIPO']
                TIPOS_VALIDOS = ['ENTERO', 'DECIMAL']

                for par in PARAMETROS_OBLIGATORIOS:
                    if par not in parametro.keys():
                        return fuente, False, 'El parametro {parametro} es obligatorio'.format(par)
                
                if parametro['TIPO'].upper() not in TIPOS_VALIDOS:
                    return fuente, False, 'El parametro TIPO es invalido. debe ser uno de los siguientes valores: {valores}'.format(TIPOS_VALIDOS)


                columnas = [int(columna) - 1 for columna in parametro['COLUMNAS'].split(',')]
                tipo = parametro['TIPO'].upper()
                for c in columnas:
                    expresion = '([0-9]+)[\.\,]([0-9]*)'
                    expresion_int = '^[0-9]+$'
                    if tipo == "ENTERO":

                        fuente = fuente.withColumn(fuente.columns[c], regexp_replace(fuente.columns[c], expresion, '$1'))

                    if tipo == "DECIMAL":

                        fuente = fuente.withColumn(fuente.columns[c], regexp_replace(fuente.columns[c], expresion_int, "0"))
                        fuente = fuente.withColumn(fuente.columns[c], regexp_replace(fuente.columns[c], expresion, '$2'))

                    fuente = fuente.withColumn(fuente.columns[c], floor(fuente.columns[c]))

                return fuente, True, "Transformacion aplicada correctamente"
            except Exception as ex:
                print(ex)
                return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)


