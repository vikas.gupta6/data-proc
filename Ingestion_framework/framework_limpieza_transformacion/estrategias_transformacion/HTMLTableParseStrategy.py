# -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.types import StructType,StructField, StringType
from Constantes import *
import pandas as pd

class HTMLTableParseStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(HTMLTableParseStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar un string en una posicion fija en todas las filas de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''

        PARAMETROS_OBLIGATORIOS = ['NUMERO_TABLA']

        for par in PARAMETROS_OBLIGATORIOS:
           if par not in parametro.keys():
               return fuente, False, 'El parametro {parametro} es obligatorio'.format(parametro=par)
        
        numero_tabla = int(parametro['NUMERO_TABLA'])-1

        try:
             # Transformar dataframe en lista python
            fuente = fuente.rdd.flatMap(lambda x : x).collect()
            # Obtener string con html
            fuente = ' '.join(fuente)
            # Crear dataframe de pandas a partir del string HTML
            fuente  = pd.read_html(fuente)

            # Definir todos los campos como String
            nuevas_columnas = []
            for columna in fuente[numero_tabla].columns:
                nuevas_columnas.append(StructField('{0}'.format(columna), StringType(), True))
            nuevo_schema = StructType(nuevas_columnas)

            fuente = self.get_sql_context().createDataFrame(fuente[0], nuevo_schema)

            return fuente, True, MSJ_TRANSFORMACION_CORRECTA
        except Exception as ex:
            return fuente, False, MSJ_TRANSFORMACION_FALLIDA.format(ex)