# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy \
    import ITransformacionStrategy
from pyspark.sql.functions import udf, lit

class CampoValorFijoStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(CampoValorFijoStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar un caracter en una posicion fija en todas las filas de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        if 'POSICION' in parametro.keys():
            pos = int(parametro['POSICION'])
            nom = 'col_{}_fijo'
            if 'VALOR' in parametro.keys():
                val = parametro['VALOR']
                columnas = fuente.columns
                columnas = columnas.insert(pos-1, nom.format(pos-1))
                fuente = fuente.withColumn(nom.format(pos-1), lit(val)).select(columnas)
                return fuente, True, "Tansformacion aplicada correctamente"
            else:
                return fuente, False, "El parametro VALOR no se encontro"
        else:
            return fuente, False, "El parametro POSICION no se encontro"