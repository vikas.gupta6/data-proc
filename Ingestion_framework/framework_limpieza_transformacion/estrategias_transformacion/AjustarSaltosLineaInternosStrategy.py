# -*- coding: UTF-8 -*-
from math import trunc
from os import sep
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
import re
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes
from pyspark.sql import functions as f
from pyspark.sql.window import Window
from pyspark.sql.types import StringType, StructField, StructType
from Ingestion_framework.utilitarios.Utiles import \
    Utiles

class AjustarSaltosLineaInternosStrategy(ITransformacionStrategy):

    _REGEX_NOMBRE_RUTA = '([\w:\/\-\.]+)\/(.*)'
    
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(AjustarSaltosLineaInternosStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
        file_name = 'gs://testing-ingestion/config_files/config.json'
        self.config = Utiles().load_config(file_name)
        self.bucket_name = self.config['source_bucket']
    

    def aplicar_transformacion(self, fuente, parametro):

        try:
            archivo_a_cargar = parametro['ARCHIVO']
            separador = parametro['SEPARADOR']
            separador_objetivo = parametro['SEPARADOR_OBJETIVO']
            charset = parametro['CHARSET'] if 'CHARSET' in parametro else 'UTF-8'

            posibles_separadores_falsos = ['~', '¬', '=', '^', '`', '|']

            nombre_archivo = re.match(self._REGEX_NOMBRE_RUTA, archivo_a_cargar).group(2)
            ruta_archivo = re.match(self._REGEX_NOMBRE_RUTA, archivo_a_cargar).group(1)

            def ajustar_searador_entre_comillas(linea):
                if '"' not in linea:
                    return linea
                comilla_abierta = False
                nueva_linea = ""
                for caracter in linea:
                    if caracter != separador and caracter != '"':
                        nueva_linea = nueva_linea + caracter
                    elif caracter == '"':
                        if comilla_abierta:
                            comilla_abierta = False
                        else:
                            comilla_abierta = True
                    elif caracter == separador and not comilla_abierta:
                        nueva_linea = nueva_linea + caracter
                return nueva_linea

            udf_ajustar_separadores = f.udf(lambda z: ajustar_searador_entre_comillas(z),StringType())

            
            # Cargar archivo con separador falso, para cargar csv de 1 columna
            caracter_falso = None
            for caracter_falso_posible in posibles_separadores_falsos:
                valido = True
                for columna in fuente.columns:
                    encontrados = int(fuente.filter(f.col(columna).like('%{}%'.format(caracter_falso_posible))).count())
                    if encontrados > 0:
                        valido = False
                        break
                if valido:
                    caracter_falso = caracter_falso_posible
                    break
            if caracter_falso is None:
                raise RuntimeError('No se encontró separador falso posible')

            fuente = Utiles().cargar_archivo(self.bucket_name,nombre_archivo, ruta_archivo, caracter_falso, encoding=charset)

            
            # reemplazar caracteres no imprimibles
            fuente = fuente.withColumn(fuente.columns[0], f.regexp_replace(fuente.columns[0], 'u[^ -~{}]+'.format(separador), ''))

            # reemplazar separador objetivo por nada, para evitar fallos posteriores
            if separador != separador_objetivo:
                fuente = fuente.withColumn(fuente.columns[0], f.regexp_replace(fuente.columns[0], separador_objetivo, ''))

            fuente = fuente.withColumn(fuente.columns[0], udf_ajustar_separadores(f.col(fuente.columns[0])))
        
            #Reemplazar comillas dobles por nada
            fuente = fuente.withColumn(fuente.columns[0], f.regexp_replace(fuente.columns[0], '"', ''))
            #Obtener la cantidad de columnas maxima
            separador_buscar = '\{}'.format(separador) if separador in posibles_separadores_falsos else separador
            cantidad = int(fuente.withColumn('cantidad_separador', f.size(f.split(fuente.columns[0], separador_buscar))).agg({'cantidad_separador':'max'}).collect()[0][0])

            cantidad_min = int(fuente.withColumn('cantidad_separador_min', f.size(f.split(fuente.columns[0], separador_buscar))).agg({'cantidad_separador_min':'min'}).collect()[0][0])

            print('========== Max={},Min={}'.format(cantidad, cantidad_min))

            if cantidad != cantidad_min:
                #Reemplazar separador por: comilla doble + separador + comilla doble
                fuente = fuente.withColumn(fuente.columns[0], f.regexp_replace(fuente.columns[0], separador, '"{}"'.format(separador)))

                #print('====== AJUSTE COMILLAS A FILAS COMPLETAS ======')
                fuente = fuente.withColumn(fuente.columns[0], 
                    f.when(f.size(f.split(fuente.columns[0], separador)) == cantidad, 
                        f.regexp_replace(fuente.columns[0], '^(.*)$', '"$1"'))
                    .otherwise(f.col(fuente.columns[0])))

                #Ajustar filas con campos incompletos
                w=Window.orderBy(f.lit(1))
                fuente = fuente.withColumn('row_number', f.row_number().over(w))
                w=Window.orderBy("row_number")

                fuente = fuente.withColumn('Anterior', f.lag(fuente.columns[0], 1).over(w))
                fuente = fuente.withColumn('Correcto', f.when((f.col(fuente.columns[0]).startswith('"')) & (f.col(fuente.columns[0]).endswith('"')), True).otherwise(False))
                fuente = fuente.withColumn('Completo', f.concat(fuente.columns[2],fuente.columns[0]))
                fuente = fuente.drop('Anterior')

                w=Window.partitionBy('Correcto').orderBy('row_number')

                fuente = fuente.withColumn('numero_error', f.row_number().over(w) % 2)
                fuente = fuente.where((fuente.Correcto == True) | (fuente.numero_error == 0)).withColumn(
                    fuente.columns[0], f.when(f.col('Correcto') == True, f.col(fuente.columns[0])).otherwise(f.regexp_replace(fuente.Completo, '^(.*)$', '"$1"')))
                fuente = fuente.drop('row_number')
                fuente = fuente.drop('Correcto')
                fuente = fuente.drop('Completo')
                fuente = fuente.drop('numero_error')

                #Reemplazar comillas dobles por nada
                fuente = fuente.withColumn(fuente.columns[0], f.regexp_replace(fuente.columns[0], '"', ''))

                
            schema = StructType([StructField('C_{}'.format(campo), StringType()) for campo in range(cantidad) ])
                
            fuente = fuente.select(f.split(fuente.columns[0], separador_buscar)).rdd.flatMap(lambda x: x).toDF(schema)

            return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex) 