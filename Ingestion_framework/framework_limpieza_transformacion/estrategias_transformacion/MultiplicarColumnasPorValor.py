# -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql.functions import col, lit
import Constantes

class MultiplicarColumnasPorValor(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(MultiplicarColumnasPorValor, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)
    
    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformacion se encarga de cambiar los saltos de línea de windows o mac a unix.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformacion (True = Transformacion ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformacion.
        '''

        PARAMETROS_OBLIGATORIOS = ['COLUMNAS', 'VALOR']

        for par in PARAMETROS_OBLIGATORIOS:
           if par not in parametro.keys():
               return fuente, False, 'El parametro {parametro} es obligatorio'.format(parametro=par)
        
        columnas = [int(columna)-1 for columna in parametro['COLUMNAS'].split(',')]
        valor = float(parametro['VALOR'])

        for columna in columnas:
            fuente = fuente.withColumn(fuente.columns[columna], col(fuente.columns[columna]) * valor)
        
        return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA