# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import lit
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion import \
    Constantes

class InsertarColumnasStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(InsertarColumnasStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de insertar columnas vacías en el dataframe en las posiciones indicadas.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        try:
            col_nm = 'CD_{}'
            pos = parametro['POSICIONES']
            pos = pos.split(',')
            pos = [int(i.strip())-1 for i in pos]
            #pos.reverse()
            pos.sort(reverse=True)
            cols = fuente.columns
            for num,p in enumerate(pos):
                cols.insert(p, col_nm.format(num))
                fuente = fuente.withColumn(col_nm.format(num), lit('')).select(cols)
            return fuente, True, Constantes.MSJ_TRANSFORMACION_CORRECTA
        except Exception as ex:
            return fuente, False, Constantes.MSJ_TRANSFORMACION_FALLIDA.format(ex) 
