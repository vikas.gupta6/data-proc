# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql import functions as f
from pyspark.sql.functions import udf, col

class SubstringCampoStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(SubstringCampoStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta Transformacion se encarga de aplicar un substring a una columna.
        Parametros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        Retorna:
            fuente: fuente transformada.
            Bool: Valor que indica si se ejecutó correctamente o no la transformacion  (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.

        '''
        columna = int(parametro['COLUMNA'])-1
        posicion_inicial = int(parametro['POSICION_INICIAL']) if 'POSICION_INICIAL' in parametro else None
        cantidad_caracteres = int(parametro['POSICION_FINAL']) if 'POSICION_FINAL' in parametro else None

        f = None

        # Si cantidad_caracteres es negativo, tomar los ultimos n caracteres
        if cantidad_caracteres < 0:
            def f(x): return x[cantidad_caracteres:]
        # Si cantidad_caracteres es positivo, tomar esa cantidad de caracteres a partir de la posicion_inicial
        if cantidad_caracteres > 0 and posicion_inicial >= 0:
            def f(x): return x[posicion_inicial:posicion_inicial+cantidad_caracteres]
        # Si cantidad_caracteres es nulo, tomar desde la posicion_inicial hasta el final de la cadena
        if cantidad_caracteres is None and posicion_inicial > 0:
            def f(x): return x[posicion_inicial:]

        f_udf = udf(f)

        try:
            fuente = fuente.withColumn(fuente.columns[columna],f_udf(col(fuente.columns[columna])) )
            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)
