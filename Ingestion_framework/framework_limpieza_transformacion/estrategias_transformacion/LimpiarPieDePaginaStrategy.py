# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy

class LimpiarPieDePaginaStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(LimpiarPieDePaginaStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de eliminar el pie de pagina de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''
        parametro = int(parametro['NUMERO_FILAS_PIE'])
        if parametro > 0:
            return self.get_sql_context().createDataFrame(fuente.collect()[:-parametro]), True, "Transformacion aplicada correctamente"
        else:
            return fuente, False, "El parametro es igual o menor a 0. Valor del parametro {0}".format(parametro)
