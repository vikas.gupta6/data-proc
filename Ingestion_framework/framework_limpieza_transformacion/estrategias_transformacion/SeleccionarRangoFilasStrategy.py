# -*- coding: UTF-8 -*-
from ITransformacionStrategy import ITransformacionStrategy
from pyspark.sql import functions as f
from pyspark.sql.functions import udf, col

class SeleccionarRangoFilasStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(SeleccionarRangoFilasStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta Transformacion se encarga de aplicar un substring a una columna.
        Parametros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        Retorna:
            fuente: fuente transformada.
            Bool: Valor que indica si se ejecutó correctamente o no la transformacion  (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.

        '''
        
        PARAMETROS_OBLIGATORIOS = ['FILA_INICIO', 'FILA_FIN']

        for par in PARAMETROS_OBLIGATORIOS:
            if par not in parametro.keys():
                return fuente, False, 'El parametro {parametro} es obligatorio'.format(parametro=par)
        
        fila_inicio = int(parametro['FILA_INICIO'])-1
        fila_fin = int(parametro['FILA_FIN'])

        try:
            fuente = self.get_sql_context().createDataFrame(fuente.collect()[fila_inicio : fila_fin])
            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)
