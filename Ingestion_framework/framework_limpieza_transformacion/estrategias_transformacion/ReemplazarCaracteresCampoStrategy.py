# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql import functions as f

class ReemplazarCaracteresCampoStrategy(ITransformacionStrategy):
    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(ReemplazarCaracteresCampoStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta Transformacion se encarga de reemplazar caracteres a una columna.
        Parametros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        Retorna:
            fuente: fuente transformada.
            Bool: Valor que indica si se ejecutó correctamente o no la transformacion  (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.

        '''
        columna = int(parametro['COLUMNA'])-1

        espacio_es_nulo = True
        if 'ESPACIO_ES_NULO' in parametro and parametro['ESPACIO_ES_NULO'] not in (True, 'True', '1'):
            espacio_es_nulo = False
        #Construir diccionario de remplazo
        caracteres_busqueda = ''
        caracteres_remplazo = ''
        caracteres_eliminar = ''
        for caracter in range(len(parametro['CARACTERES_BUSQUEDA'])):
            if parametro['CARACTERES_REEMPLAZO'][caracter] == ' ' and espacio_es_nulo:
                caracteres_eliminar = caracteres_eliminar + parametro['CARACTERES_BUSQUEDA'][caracter]
            else:
                caracteres_busqueda = caracteres_busqueda + parametro['CARACTERES_BUSQUEDA'][caracter]
                caracteres_remplazo = caracteres_remplazo + parametro['CARACTERES_REEMPLAZO'][caracter]
        try:
            if len(caracteres_busqueda) > 0:
                fuente = fuente.withColumn(fuente.columns[columna], f.translate(fuente.columns[columna], caracteres_busqueda, caracteres_remplazo))
            if len(caracteres_eliminar) > 0:
                fuente = fuente.withColumn(fuente.columns[columna], f.regexp_replace(fuente.columns[columna], '[{}]'.format(caracteres_eliminar), ''))
            return fuente, True, "Transformacion aplicada correctamente"
        except Exception as ex:
            return fuente, False, "No se pudo aplicar la transformancion - {}".format(ex)
