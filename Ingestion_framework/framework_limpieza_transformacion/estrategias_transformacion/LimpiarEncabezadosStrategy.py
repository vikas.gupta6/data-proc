# -*- coding: UTF-8 -*-
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ITransformacionStrategy import \
    ITransformacionStrategy
from pyspark.sql.functions import udf
from pyspark.sql.types import StructType, StructField, StringType

class LimpiarEncabezadosStrategy(ITransformacionStrategy):

    def __init__(self, trace_ejecucion_procesos_manager=None, verbose=False):
        super(LimpiarEncabezadosStrategy, self).__init__(trace_ejecucion_procesos_manager=trace_ejecucion_procesos_manager, verbose=verbose)

    def aplicar_transformacion(self, fuente, parametro):
        '''
        Esta transformación se encarga de eliminar el encabezado de un archivo.
        parámetros:
            fuente: Datos en formato Dataframe.
            parametro: Diccionario.

        retorna:
            fuente: fuente transformada.
            Bool: Valor booleano que indica si se ejecuto correctamente o no la transformación (True = Transformación ejecutada correctamente).
            str: Mensaje que indica si se ejecutó correctamente o no la transformación.
        '''

        cantidad_filas = int(parametro['NUMERO_FILAS_CABECERA'])
        if cantidad_filas > 0:
            # schema_actual = fuente.schema
            # fuente_nueva = fuente.rdd.mapPartitionsWithIndex(lambda id_x, iter: list(iter)[cantidad_filas:] if(id_x == 0) else iter)
            # return self.get_sql_context().createDataFrame(fuente_nueva, schema=schema_actual), True, "Transformacion aplicada correctamente"
            return self.get_sql_context().createDataFrame(fuente.toPandas().iloc[cantidad_filas:,:]), True, "Transformacion aplicada correctamente"
        else:
            return fuente, False, "El parametro es igual o menor a 0. Valor del parametro {0}".format(parametro)




