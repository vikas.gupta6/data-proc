# -*- coding: UTF-8 -*-

__version__ = "0.1"

import sys
import os
import re
import csv

sys.path.append('utilitarios')
sys.path.append('api_traza_auditoria')

from Ingestion_framework.utilitarios.Utiles import \
    Utiles
from Ingestion_framework.api_traza_auditoria.TraceEjecucionProcesosManager import \
    TraceEjecucionProcesosManager
# from fabricas.HiveDAOFactory import HiveDAOFactory
from Ingestion_framework.framework_limpieza_transformacion.fabricas.BigQueryDAOFactory import \
     BigQueryDAOFactory
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.LimpiarEncabezadosStrategy import \
    LimpiarEncabezadosStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.CambioCodificacionStrategy import \
    CambioCodificacionStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.AjusteSaltoLineaStrategy import \
    AjusteSaltoLineaStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.CambioSeparadorStrategy import \
    CambioSeparadorStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.LimpiarPieDePaginaStrategy import \
    LimpiarPieDePaginaStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.CampoValorFijoStrategy import \
    CampoValorFijoStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.FijoASeparadorStrategy import \
    FijoASeparadorStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.QuitarCerosIzquierdaStrategy import \
    QuitarCerosIzquierdaStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ReemplazarCaracteresEspecialesStrategy import \
    ReemplazarCaracteresEspecialesStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.InsertarColumnasStrategy import \
    InsertarColumnasStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.TratarFechaHoraStrategy import \
    TratarFechaHoraStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.EliminarCamposVaciosStrategy import \
    EliminarCamposVaciosStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ConcatenarColumnasStrategy import \
    ConcatenarColumnasStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ReemplazarCaracteresCampoStrategy import \
    ReemplazarCaracteresCampoStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.AjustarSaltosLineaInternosStrategy import \
    AjustarSaltosLineaInternosStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.EliminarColumnasStrategy import \
    EliminarColumnasStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.ExcelACSVStrategy import \
    ExcelACSVStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.DuplicarColumnasStrategy import \
    DuplicarColumnasStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.SubstringCampoStrategy import \
    SubstringCampoStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.LPADStrategy import \
    LPADStrategy
from Ingestion_framework.framework_limpieza_transformacion.estrategias_transformacion.RPADStrategy import \
    RPADStrategy


try:
    from argparse import ArgumentParser
except ModuleNotFoundError:
    # python2.4
    from optparse import OptionParser


class FrameworkLimpiezaTransformacion:
    _dao_factory = None
    _estrategia_transformacion = None
    _trace_ejecucion_procesos_manager = None
    _config = None
    _spark_context = None
    _sql_context = None

    def __init__(self, trace_ejecucion_procesos_manager):
        self.set_trace_ejecucion_procesos_manager(trace_ejecucion_procesos_manager)
        self.set_config(Utiles().load_config('gs://testing-ingestion/config_files/config.json'))
        # if self.get_config()['tipo_almacenamiento'] == 'HIVE':
        #     self._dao_factory = HiveDAOFactory(self.get_trace_ejecucion_procesos_manager())
        if self.get_config()['tipo_almacenamiento'] == 'BIGQUERY':
            self._dao_factory = BigQueryDAOFactory(self.get_trace_ejecucion_procesos_manager())
        else:
            raise NotImplementedError('El tipo de almacenamiento "{tipo_alm}" no esta soportado.'.format(
                tipo_alm=self.get_config()['tipo_almacenamiento']))
        self._spark_context, self._sql_context = Utiles().get_spark_session()

    def obtener_fuentes_asociadas(self, archivo):
        fuentes_asociadas = self._dao_factory.obtener_archivo_fuente_dao().obtener_fuentes_archivo(archivo)
        return fuentes_asociadas

    def obtener_datos_fuentes(self):
        datos_fuentes = self._dao_factory.obtener_archivo_fuente_dao().obtener_datos()
        return datos_fuentes

    def obtener_fuente_cloudera_nombre(self, nombre):
        fuente_cloudera = self._dao_factory.obtener_fuentes_cloudera_dao().obtener_nombre_fuente(nombre)
        return fuente_cloudera

    def obtener_fuente_cloudera_codigo(self, codigo):
        fuente_cloudera = self._dao_factory.obtener_fuentes_cloudera_dao().obtener_informacion_fuente(codigo)
        return fuente_cloudera

    def obtener_campos_fuentes_cloudera(self, codigo):
        campos_fuente = self._dao_factory.obtener_campos_fuentes_cloudera_dao().obtener_campos_fuente(codigo)
        return campos_fuente

    def obtener_transformaciones_asociadas_archivo_fuente(self, fuente_cruda, fuente_final, etapa='Limpieza'):
        transformaciones_asociadas_archivo_fuente = self._dao_factory.obtener_transformacion_archivo_dao().obtener_transformaciones_archivo(
            fuente_cruda, fuente_final, etapa)
        return transformaciones_asociadas_archivo_fuente

    def obtener_parametros_transformacion(self, codigo_fuente_cruda, codigo_fuente_final, etapa, orden):
        parametros_transformacion = self._dao_factory.obtener_parametro_transformacion_dao().obtener_parametros_transformacion(
            codigo_fuente_cruda, codigo_fuente_final, etapa, orden)
        return parametros_transformacion

    def arreglar_meses(self, fecha):
        fecha = fecha.lower()
        if not fecha.isdigit():
            lmeses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre',
                      'noviembre', 'diciembre']
            meses = {'ene': '01', 'feb': '02', 'mar': '03', 'abr': '04', 'may': '05', 'jun': '06', 'jul': '07',
                     'ago': '08', 'sep': '09', 'oct': '10', 'nov': '11', 'dic': '12'}
            for x in lmeses:
                lista_mes = [x[:i] for i in range(3, len(x) + 1)]
                lista_contiene = [i for i in lista_mes if i in fecha]
                if len(lista_contiene) > 0:
                    fecha = meses[x[:3]]
        return fecha

    def tratamiento_fecha(self, fecha, formato_fecha):
        c = 0
        dia, mes, anio = [], [], []
        for i in formato_fecha:
            if i.upper() in ("Y", "A"):
                anio.append(c)
            elif i.upper() == "M":
                mes.append(c)
            elif i.upper() == "D":
                dia.append(c)
            c += 1
        r = []
        for i in [anio, mes, dia]:
            if len(i) > 1:
                r.append(fecha[i[0]:i[-1] + 1])
            elif len(i) == 1:
                r.append(fecha[i[0]])
            elif len(i) == 0:
                r.append("01")
        r[1] = self.arreglar_meses(r[1])
        return r

    def procesar_nombres(self, nombre_archivo, datos_fuentes, etapa):
        lista_fuentes = []
        registro = []
        extension = nombre_archivo.split(".")[-1]
        extension = "." + extension
        for dato in datos_fuentes:
            try:
                tmp = re.match(dato.get_nombre_archivo_origen(), nombre_archivo).group(0)
                fecha = re.match(dato.get_nombre_archivo_origen(), nombre_archivo).group(1)
                if tmp == nombre_archivo:
                    registro.append({'dato': dato, 'regex_result': tmp, 'fecha': fecha})
            except IndexError:
                pass
            except AttributeError:
                pass
        if len(registro) == 0:
            return lista_fuentes
        else:
            for record in registro:
                if etapa == 'Limpieza':
                    fuente_cloudera = flt.obtener_fuente_cloudera_codigo(record['dato'].get_codigo_fuente_cruda())
                    nombre_cloudera = fuente_cloudera.get_nombre_tabla()
                    campos_fuente_cloudera = flt.obtener_campos_fuentes_cloudera(
                        record['dato'].get_codigo_fuente_cruda())
                else:
                    fuente_cloudera = flt.obtener_fuente_cloudera_codigo(record['dato'].get_codigo_fuente_final())
                    nombre_cloudera = fuente_cloudera.get_nombre_tabla()
                    campos_fuente_cloudera = flt.obtener_campos_fuentes_cloudera(
                        record['dato'].get_codigo_fuente_final())
                anio, mes, dia = self.tratamiento_fecha(record['fecha'], record['dato'].get_formato_fecha())
                if isinstance(dia, list):
                    dia = dia[0]
                    nombre_final = nombre_cloudera + "_" + "".join([anio, mes, dia])  # + extension
                else:
                    nombre_final = nombre_cloudera + "_" + "".join([anio, mes, dia])  # + extension
                lista_fuentes.append({'dato': record['dato'], 'nombre_cloudera': nombre_final,
                                      'campos_fuentes_cloudera': campos_fuente_cloudera,
                                      'fuentes_cloudera': fuente_cloudera})
        return lista_fuentes

    def set_trace_ejecucion_procesos_manager(self, trace_ejecucion_procesos_manager):
        self._trace_ejecucion_procesos_manager = trace_ejecucion_procesos_manager

    def get_trace_ejecucion_procesos_manager(self):
        return self._trace_ejecucion_procesos_manager

    def set_config(self, config):
        self._config = config

    def get_config(self):
        return self._config

    def get_spark_context(self):
        return self._spark_context

    def set_spark_context(self, spark_context):
        self._spark_context = spark_context

    def get_sql_context(self):
        return self._sql_context

    def set_sql_context(self, sql_context):
        self._sql_context = sql_context


class ProjectError(Exception):
    '''
    Todos los "raise" deben derivar de esta clase
    '''
    pass


class NoFuentesAsociadas(ProjectError):
    '''
    Raise cuando no hay fuentes asociadas a un archivo
    '''
    pass


class InterruptedError(ProjectError):
    '''
    Raise para interrumpir la ejecucion
    '''
    pass


if __name__ == '__main__':
    ruta = sys.argv[1].split(",")[0]
    archivo = sys.argv[1].split(",")[1]
    etapa = "Limpieza"


    REGEX_NOMBRE_RUTA = '([\w:\/\-\.]+\/){0,1}(.*)'

    nombre_archivo_tratar = archivo
    ruta_archivo_tratar = '/landings/davivienda/pre-landing/transformaciones_cloudera/' if ruta is None or ruta == '' else ruta
    ruta_archivo_tratar = ruta_archivo_tratar[:-1] if ruta_archivo_tratar.endswith('/') else ruta_archivo_tratar

    with TraceEjecucionProcesosManager('FW-CL', 'LimpiezaTransformacion', verbose=True, verbose_dao=False,
                                       nombre_proceso='Proceso de {0} para archivo {1}'.format(etapa,
                                                                                               nombre_archivo_tratar)) as tepm:

        # Diccionario de transformaciones
        transformaciones = {
            'LIMPIAR_ENCABEZADOS': LimpiarEncabezadosStrategy,
            'CAMBIO_CODIFICACION': CambioCodificacionStrategy,
            'AJUSTE_SALTO_LINEA': AjusteSaltoLineaStrategy,
            # 'CAMBIO_SEPARADOR': CambioSeparadorStrategy,
            'LIMPIAR_PIE_PAGINA': LimpiarPieDePaginaStrategy,
            'CAMPO_VALOR_FIJO': CampoValorFijoStrategy,
            'FIJO_A_SEPARADOR': FijoASeparadorStrategy,
            'QUITAR_CEROS_IZQUIERDA': QuitarCerosIzquierdaStrategy,
            'REEMPLAZAR_CARACTERES_ESPECIALES': ReemplazarCaracteresEspecialesStrategy,
            'INSERTAR_COLUMNAS': InsertarColumnasStrategy,
            'TRATAR_FECHA_HORA': TratarFechaHoraStrategy,
            'ELIMINAR_CAMPOS_VACIOS': EliminarCamposVaciosStrategy,
            "CONCATENAR_COLUMNAS": ConcatenarColumnasStrategy,
            'REEMPLAZAR_CARACTERES_COLUMNA': ReemplazarCaracteresCampoStrategy,
            'AJUSTAR_SALTOS_LINEA_INTERNOS': AjustarSaltosLineaInternosStrategy,
            'ELIMINAR_COLUMNAS': EliminarColumnasStrategy,
            'EXCEL_A_CSV': ExcelACSVStrategy,
            'DUPLICAR_COLUMNAS': DuplicarColumnasStrategy,
            'SUBSTRING_CAMPO': SubstringCampoStrategy,
            'LPAD': LPADStrategy,
            'RPAD': RPADStrategy
        }

        tepm.insert_trace_ejecucion_proceso(
            'Inicia proceso de {0} para archivo {1}'.format(etapa, nombre_archivo_tratar))
        flt = FrameworkLimpiezaTransformacion(tepm)

        processing_folder = ruta_archivo_tratar + '/' + flt.get_config()['folder_procesamiento_hdfs']
        failed_folder = ruta_archivo_tratar + '/' + flt.get_config()['folder_fallos_hdfs']
        starting_folder = ruta_archivo_tratar + '/' + flt.get_config()['folder_starting']
        bucket_name = flt.get_config()['source_bucket']
        starting_folder_tmp = starting_folder + nombre_archivo_tratar
        ruta_archivo_tratar_tmp = ruta_archivo_tratar + '/' + nombre_archivo_tratar

        # Mover archivo a folder de procesamiento
        if Utiles().es_archivo(bucket_name, starting_folder_tmp):
            Utiles().mover_archivo(processing_folder, bucket_name, starting_folder_tmp)
        elif Utiles().es_archivo(bucket_name, ruta_archivo_tratar_tmp):
            Utiles().mover_archivo(processing_folder, bucket_name, ruta_archivo_tratar_tmp)
        else:
            raise RuntimeError(
                'No existe el archivo "{}". Se busco en las carpetas "{}" y "{}"'.format(nombre_archivo_tratar,starting_folder_tmp,ruta_archivo_tratar_tmp))

        # Obtener fuentes asociadas al archivo recibido.
        tepm.insert_trace_ejecucion_proceso(
            'Obteniendo las fuentes asociadas al archivo {0}'.format(nombre_archivo_tratar))
        datos_fuentes = flt.obtener_datos_fuentes()
        lista_fuentes = flt.procesar_nombres(nombre_archivo_tratar, datos_fuentes, etapa)

        if len(lista_fuentes) != 0:

            tepm.insert_trace_ejecucion_proceso('Se identificaron {0} fuentes asociadas'.format(len(lista_fuentes)))

            # Crear folder .Processing y .Failed si no existen
            fs = flt.get_spark_context()._jvm.org.apache.hadoop.fs.FileSystem.get(
                flt.get_spark_context()._jsc.hadoopConfiguration())
            for folder in [processing_folder, failed_folder]:
                Utiles().crear_carpeta(bucket_name,folder)

            # Para cada asociacion de archivo, hacer:
            for fuente in lista_fuentes:
                # Obtener transformaciones asociadas a la fuente.
                tepm.insert_trace_ejecucion_proceso(
                    "Obteniendo las transformaciones asociadas a la fuente {0}".format(nombre_archivo_tratar))
                transformaciones_archivo = flt.obtener_transformaciones_asociadas_archivo_fuente(
                    fuente['dato'].get_codigo_fuente_cruda(), fuente['dato'].get_codigo_fuente_final(), etapa)
                # Para cada asociacion obtener las transformaciones a aplicar.

                # Cargar archivo en pyspark dataframe
                if str(nombre_archivo_tratar).split('.')[-1].upper() in ('XLS', 'XLSX'):
                    tepm.insert_trace_ejecucion_proceso(
                        'El archivo {} se encuentra en formato excel, la primera transformacion debe ser EXCEL_A_CSV para definir la hoja a tratar'.format(
                            nombre_archivo_tratar))
                    file_df = None

                    if ' ' in nombre_archivo_tratar:
                        tepm.insert_trace_ejecucion_proceso(
                            'El archivo "{}" contiene espacios, para que sea compatible con la lectura de archivos excel, se deben cambiar por _'.format(
                                nombre_archivo_tratar))
                        nombre_archivo_tratar_ajustado = str(nombre_archivo_tratar).replace(' ', '_')
                        nombre_archivo_tratar_tmp = nombre_archivo_tratar + '/' + processing_folder
                        Utiles().mover_archivo(bucket_name,nombre_archivo_tratar_tmp, processing_folder,
                                               nombre_archivo_tratar_ajustado)
                        nombre_archivo_tratar = nombre_archivo_tratar_ajustado
                else:
                    try:
                        tepm.insert_trace_ejecucion_proceso(
                            'Cargando archivo {} en dataframe'.format(nombre_archivo_tratar))
                        file_df = Utiles().cargar_archivo(bucket_name, nombre_archivo_tratar, processing_folder,
                                                          fuente['dato'].get_delimitador(), multilinea=True)
                    except Exception as ex:
                        tepm.insert_trace_ejecucion_proceso(
                            'Falla la carga del archivo al dataframe. Esto puede deberse a que el archivo contiene saltos de linea internos. Se intenta cargar como archivo de texto, no CSV')
                        file_df = Utiles().cargar_archivo(bucket_name,archivo, processing_folder, '')

                tepm.insert_trace_ejecucion_proceso(
                    "Se obtuvieron {0} transformaciones para la fuente {1}".format(len(transformaciones_archivo),
                                                                                   nombre_archivo_tratar))
                for transformacion in transformaciones_archivo:
                    params = {}
                    tepm.insert_trace_ejecucion_proceso(
                        "Obteniendo los parametros asociados a la transformacion {0}".format(
                            transformacion.get_transformacion()))
                    parametros_transformacion = flt.obtener_parametros_transformacion(
                        transformacion.get_codigo_fuente_cruda(), transformacion.get_codigo_fuente_final(),
                        transformacion.get_etapa(), transformacion.get_orden())
                    if len(parametros_transformacion) == 0:
                        err = "No se encontro ningun parametro para la transformacion {0}".format(
                            transformacion.get_transformacion())
                        tepm.insert_trace_ejecucion_proceso(err)
                    for parametro in parametros_transformacion:
                        # Para cada transformacion obtener los parametros requeridos.
                        params[parametro.get_parametro()] = parametro.get_valor()
                    if transformacion.get_transformacion() == 'AJUSTAR_SALTOS_LINEA_INTERNOS':
                        params['ARCHIVO'] = processing_folder+nombre_archivo_tratar
                        params['SEPARADOR'] = fuente['dato'].get_delimitador()
                        params['SEPARADOR_OBJETIVO'] = fuente['fuentes_cloudera'].get_delimitador_campo()
                    if transformacion.get_transformacion() == 'EXCEL_A_CSV':
                        params['RUTA_ARCHIVO'] = processing_folder
                        params['NOMBRE_ARCHIVO'] = nombre_archivo_tratar
                    vls = params.values()
                    mnj = "Aplicando la transformacion '{0}' con valores de parametros : {1} a la fuente '{2}' para la etapa '{3}'".format(
                        transformacion.get_transformacion(), vls, nombre_archivo_tratar, transformacion.get_etapa())
                    tepm.insert_trace_ejecucion_proceso(mnj)
                    # Aplicar las transformaciones requeridas.
                    print("printing file df and params")
                    file_df.show()
                    print(params)
                    file_df, status, mnj = transformaciones[
                        transformacion.get_transformacion()]().aplicar_transformacion(file_df, params)

                    print("showing the data")
                    file_df.show()

                    if status:
                        tepm.insert_trace_ejecucion_proceso(mnj)
                    else:
                        tepm.insert_trace_ejecucion_proceso(mnj)
                        Utiles().salvar_archivo(bucket_name, file_df, nombre_archivo_tratar, failed_folder,
                                                fuente['dato'].get_delimitador(), flt.get_config()['folder_temporal'])
                        mnj2 = "Archivo movido a la carpeta {0} con el nombre {1}".format(failed_folder,
                                                                                          nombre_archivo_tratar)
                        raise InterruptedError(('{0}. {1}').format(mnj, mnj2))
                    # file_df.show(20, False)
                # Mover archivo tratado a zona de landings.
                tepm.insert_trace_ejecucion_proceso(
                    "Guardando fuente procesada {0} en {1}/{2}".format(nombre_archivo_tratar,
                                                                       flt.get_config()["asociacion_database_landings"][
                                                                           fuente[
                                                                               'fuentes_cloudera'].get_database_destino()],
                                                                       fuente['nombre_cloudera']))
                Utiles().salvar_archivo(bucket_name, file_df, fuente['nombre_cloudera'],flt.get_config()["asociacion_database_landings"][fuente['fuentes_cloudera'].get_database_destino()],
                                        fuente['fuentes_cloudera'].get_delimitador_campo(),
                                        flt.get_config()['folder_temporal'])
            Utiles().eliminar_archivo(bucket_name,nombre_archivo_tratar, processing_folder)
        else:
            ("We are in final else")
            try:
                ruta_destino = flt.get_config()["asociacion_prelandings_landings"][ruta_archivo_tratar]
                err = "No se encontro ninguna expresion regular que concuerde con el nombre de archivo '{0}'. Se mueve archivo al landing asociado.({1})".format(
                    nombre_archivo_tratar, ruta_destino)
                tepm.insert_trace_ejecucion_proceso(err)
                archivo_ruta_process=nombre_archivo_tratar+ruta_archivo_tratar+processing_folder
                Utiles().mover_archivo(ruta_destino,bucket_name,archivo_ruta_process)
            except KeyError as err:
                err = "No se encontro ninguna expresion regular que concuerde con el nombre del archivo '{0}' y la ruta '{1}' no se encuentra parametrizada, Se mueve el archivo a carpeta de Fallidos".format(
                    nombre_archivo_tratar, ruta_archivo_tratar)
                tepm.insert_trace_ejecucion_proceso(err)
                archivo_ruta_process = nombre_archivo_tratar+ruta_archivo_tratar+processing_folder
                Utiles().mover_archivo(failed_folder,bucket_name,archivo_ruta_process)
                raise InterruptedError(err)
