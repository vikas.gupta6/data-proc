# -*- coding: UTF-8 -*-
import sys
import uuid
from threading import Thread
import os

try:
    from airflow.api.client.local_client import Client as dag_client
except:
    os.system("pip install 'apache-airflow[celery]==2.3.3'")

import time

sys.path.append('utilitarios')
sys.path.append('api_traza_auditoria')

from Ingestion_framework.utilitarios.Utiles import \
    Utiles
from Ingestion_framework.api_traza_auditoria.TraceEjecucionProcesosManager \
    import TraceEjecucionProcesosManager


class FileInspector:
    config = None
    folders = list()
    ruta_starting = None
    bucket = None
    tepm = None

    spark_context = None
    sql_context = None
    base_fs = None
    file_system = None

    def __init__(self, tepm):
        file_name = 'gs://testing-ingestion/config_files/inspector_config.json'
        self.config = Utiles().load_config(file_name)
        self.ruta_starting = self.config['folder_starting']
        self.folders = self.config['prelandings']
        self.bucket = self.config['prelandings_bucket']
        self.data_composer = self.config['data_composer']
        self.composer_location = self.config['Location']
        self.tepm = tepm

    def tratar_archivo(self, ruta, archivo):
        path = ruta + '/' + self.ruta_starting+'/'
        print(path)
        archivo_nombre = ruta + '/' + archivo
        print(archivo_nombre)
        if Utiles().es_archivo(self.bucket, archivo_nombre):
            self.tepm.insert_trace_ejecucion_proceso(
                'Se encuentra archivo "{0}" en ruta "{1}"'.format(archivo, ruta))
            Utiles().crear_carpeta(self.bucket, path)
            Utiles().mover_archivo(path, self.bucket, archivo_nombre)
            os.system(
                'gcloud composer environments run ' + self.data_composer + ' --location ' + self.composer_location + ' dags trigger -- Run_FrameworkLimpiezaTransformacion_job --conf \'{"ruta":"' + ruta + '", "archivo":"' + archivo + '"}\'')
        else:
            print('{} es una carpeta, no se analiza'.format(archivo))

    def validar_carpeta(self, ruta):
        print ('===== Validando carpeta: "{}" ====='.format(ruta))
        # Obtener lista de elementos (archivos y carpetas del folder prelanding)
        archivos = Utiles().get_gcs_files_list(self.bucket, ruta)
        print("archivos are like below \n", archivos)
        procesos = [Thread(target=self.tratar_archivo, args=(ruta, archivo)) for archivo in archivos]

        for proceso in procesos:
            proceso.start()
            time.sleep(1)

        for proceso in procesos:
            proceso.join()

    def validar_carpetas(self):
        procesos = [Thread(target=self.validar_carpeta, args=(folder,)) for folder in self.folders]

        for proceso in procesos:
            proceso.start()

        for proceso in procesos:
            proceso.join()


# def inspector_caller():
if __name__ == '__main__':
    with TraceEjecucionProcesosManager('FW-CL-INSPECTOR', 'FileInspector', verbose=True,
                                       verbose_dao=False,
                                       nombre_proceso='Inspeccion de archivos en prelandings') \
            as tepm:
        fi = FileInspector(tepm)
        fi.validar_carpetas()
